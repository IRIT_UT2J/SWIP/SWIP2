/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.irit.swip2.WS;

import fr.irit.swip2.SWIP;
import fr.irit.swip2.SWIPWorkflow;
import fr.irit.swip2.nltopivot.parser.model.DependencyTree;
import fr.irit.swip2.pivottomappings.exceptions.PatternsParsingException;
import fr.irit.swip2.pivottomappings.model.MappingsAnswerFormulation;
import fr.irit.swip2.pivottomappings.model.patterns.Pattern;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import org.json.JSONArray;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Fabien
 */
@Path("SWIP")
public class SWIPWebServices {
	
	private static final Logger LOGGER = LogManager.getLogger(SWIPWebServices.class);
    
    public SWIPWebServices(){
    }
    
    
    
    private Response generateResponse(String ret){
        return Response
            .status(200)
            .header("Access-Control-Allow-Origin", "*")
            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
            .header("Access-Control-Allow-Credentials", "true")
            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
            .header("Access-Control-Max-Age", "1209600")
            .entity(ret)
            .build();
    }
    
    @GET
    @Path("getProjects")
    @Produces("application/json")
    public Response getProjects(){
        JSONObject response = new JSONObject();
        ArrayList<JSONObject> projects = new ArrayList<>();
        try {
            for(String projectName: SWIP.swList.keySet()){
                SWIPWorkflow sw = SWIP.swList.get(projectName);
                projects.add(sw.toJSON());
            }
            response.put("projects", projects);
        } catch (JSONException ex) {
            LOGGER.error("ERROR DURING JSON GENERATOR getProjects WS");
        }
        return this.generateResponse(response.toString());
        
    }
    
    @GET    
    @Path("/{project}/ask")
    @Produces("application/json")
    public Response query(@PathParam("project") String projectName, @QueryParam("q") String inputQuery) {
        LOGGER.info("QUERY ON "+projectName+" --> \n \t "+inputQuery);
        SWIPWorkflow sw = SWIP.swList.get(projectName);
        MappingsAnswerFormulation maf = sw.getAnswers(inputQuery);
        String result = "";
        if(maf != null){
            result = maf.toJSON();
        }
        LOGGER.info("JSON RETURN ASK : \n"+result);        
        return this.generateResponse(result);
    }
    
    @POST    
    @Path("/{project}/ask")
    @Produces("application/json")
    public Response postQuery(@PathParam("project") String projectName, String inputQuery) {
    	//, @QueryParam("q") String inputQuery
        LOGGER.info("POSTED QUERY ON "+projectName+" --> \n \t "+inputQuery);
        SWIPWorkflow sw = SWIP.swList.get(projectName);
        MappingsAnswerFormulation maf = sw.getAnswers(inputQuery);
        String result = "";
        if(maf != null){
            result = maf.toJSON();
        }
        LOGGER.info("JSON RETURN ASK : \n"+result);        
        return this.generateResponse(result);
    }
    
    @GET    
    @Path("/{project}/getPivot")
    @Produces("application/json")
    public Response getPivot(@PathParam("project") String projectName, @QueryParam("q") String inputQuery) {
        LOGGER.info("GET PIVOT ON "+projectName+" --> \n \t "+inputQuery);
        SWIPWorkflow sw = SWIP.swList.get(projectName);
        JSONObject results = new JSONObject();
        
        HashMap<String, String> ners = sw.getNER(inputQuery);
        LOGGER.debug("NER : "+ners);
        try {
            results.put("NER", ners);
        } catch (JSONException ex) {
            LOGGER.error("ERROR adding NER to request result : " +ex.getMessage());
            LOGGER.error(ex.getStackTrace());
        }
        
        DependencyTree dt = sw.getDependenciyTree(inputQuery);
        LOGGER.debug("Dependency Tree :");
        LOGGER.debug(dt);
        try {
            results.put("Dependencies", dt.toString());
        } catch (JSONException ex) {
            LOGGER.error("ERROR adding DependencyTree to request result : " +ex.getMessage());
            LOGGER.error(ex.getStackTrace());
        }
        
        String pivotQuery = sw.getPivotQuery(dt);
        LOGGER.debug("PivotQuery : "+pivotQuery);
        try {
            results.put("Pivot", pivotQuery);
        } catch (JSONException ex) {
            LOGGER.error("ERROR adding PivotQuery to request result : " +ex.getMessage());
            LOGGER.error(ex.getStackTrace());
        }
        
        return this.generateResponse(results.toString());
    }
    
    
    @GET    
    @Path("/{project}/getPatterns")
    @Produces("application/json")
    public Response query(@PathParam("project") String projectName) {
        LOGGER.debug("GET PATTERNS ON "+projectName);
        SWIPWorkflow sw = SWIP.swList.get(projectName);
        
        JSONObject ret = new JSONObject();
        ArrayList<JSONObject> patterns = new ArrayList<>();
        for(Pattern p : sw.getPatterns()){
            try {
                patterns.add(p.toJSON());
            } catch (JSONException ex) {
                LOGGER.error("ERROR DURING EXPORTING PATTERNS TO JSON : "+ex);
            }
        }
        try {
            ret.put("patterns", patterns);
        } catch (JSONException ex) {
        	LOGGER.error("ERROR DURING EXPORTING PATTERNS TO JSON : "+ex);
        }
        
        return this.generateResponse(ret.toString());
    }
    
    @POST  
    @Path("/{project}/setPatterns")
    @Produces("application/json")
    public Response setPatterns(@PathParam("project") String projectName, @FormParam("patterns") String updatedPatterns ) {
        LOGGER.debug("SET PATTERNS ON "+projectName);
        LOGGER.debug(" -> "+updatedPatterns);
        SWIPWorkflow sw = SWIP.swList.get(projectName);
        
        JSONObject ret = new JSONObject();
        
        try {
            JSONObject uPatterns = new JSONObject(updatedPatterns);
            JSONArray allPatterns = uPatterns.getJSONArray("patterns");
            
            
            ArrayList<Pattern> newPatterns = new ArrayList<>();
            
            for(int i = 0; i < allPatterns.length(); i++){
                JSONObject jsonPattern = allPatterns.getJSONObject(i);
                newPatterns.add(new Pattern(jsonPattern, projectName));
            }
            sw.setUpdatedPatterns(newPatterns);
            
        } catch (JSONException ex) {
            LOGGER.error("Error during parsing JSON");
        } catch (PatternsParsingException ex) {
            LOGGER.error("Error during parsing patterns");
        }
        
        return this.generateResponse(ret.toString());
    }
    

    @GET    
    @Path("/{project}/sparql")
    @Produces("application/json")
    public Response sparqlQuery(@PathParam("project") String projectName,  @QueryParam("q") String inputQuery) {
        LOGGER.debug("SPARQL query on "+projectName);
        LOGGER.debug(" --> query : "+inputQuery);
        SWIPWorkflow sw = SWIP.swList.get(projectName);

        return this.generateResponse(sw.sparqlQuery(inputQuery).toString());
    }
    
}
