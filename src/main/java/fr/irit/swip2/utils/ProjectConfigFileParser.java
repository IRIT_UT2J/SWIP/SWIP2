/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.irit.swip2.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 *
 * @author Fabien
 */
public class ProjectConfigFileParser {
    public class ProjectConfig{
        
        public String _projectName;
        public String _projectDesc;
        public Boolean _distant;
        public String _kbUri;
        public Boolean _reasonning;
    }
    
    private ProjectConfig config;
    public ProjectConfigFileParser(File projectConfigFile){
        ObjectMapper mapper = new ObjectMapper();
        if(projectConfigFile != null){
            try {
                this.config = new ProjectConfig();
                System.out.println(projectConfigFile);
                Map<String, Object> configTemp = mapper.readValue(projectConfigFile, Map.class);
                this.config._projectName = configTemp.containsKey("projectName")?configTemp.get("projectName").toString():"NULL";
                this.config._projectDesc = configTemp.containsKey("projectDesc")?configTemp.get("projectDesc").toString():"NULL";
                this.config._distant = configTemp.containsKey("distant")?((boolean)configTemp.get("distant")):false;
                this.config._kbUri = configTemp.containsKey("kbUri")?configTemp.get("kbUri").toString():"";
                this.config._reasonning = configTemp.containsKey("reasonning")?((boolean)configTemp.get("reasonning")):false;
            } catch (IOException ex) {
                System.err.println("ERROR : "+ex);
            }
            
        }
    }
    
    public String getProjectName(){
        return this.config._projectName;
    }
    public String getProjectDesc(){
        return this.config._projectDesc;
    }
    public Boolean isDistant(){
        return this.config._distant;
    }
    public String getKbUri(){
        return this.config._kbUri;
    }
    public Boolean getReasonning(){
        return this.config._reasonning;
    }
    
}
