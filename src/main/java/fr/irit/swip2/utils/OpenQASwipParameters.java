/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.irit.swip2.utils;


/**
 *
 * @author Fabien
 */
public class OpenQASwipParameters {
    public static final String NER_QUERY = "NER_QUERY";
    public static final String NER_MAP = "NER_MAP";
    public static final String DEP_GRAPH = "DEP_GRAPH";
    public static final String HEAD_NODE = "HEAD_NODE";
    public static final String PIVOT_QUERY = "PIVOT_QUERY";
}
