/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.irit.swip2.utils;

import java.util.HashMap;
import java.util.Map;
import org.apache.jena.fuseki.embedded.FusekiEmbeddedServer;
import org.apache.jena.query.Dataset;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.irit.swip2.pivottomappings.controller.Controller;

/**
 *
 * @author Fabien
 */
public class EmbeddedFuseki {
	
	private static final Logger LOGGER = LogManager.getLogger(EmbeddedFuseki.class);
    
    static private EmbeddedFuseki fusekiServer;
    static public EmbeddedFuseki getFusekiServer(){
        if(EmbeddedFuseki.fusekiServer == null){
            EmbeddedFuseki.fusekiServer = new EmbeddedFuseki();
        }
        return EmbeddedFuseki.fusekiServer;
    }
    
    
    private FusekiEmbeddedServer server;
    private FusekiEmbeddedServer.Builder serverBuilder;
    
    private HashMap<String, Dataset> datasets;
    
    public EmbeddedFuseki(){
        this.datasets = new HashMap<>();
    }
    
    public void addDataset(String name, Dataset ds){
        this.datasets.put(name, ds);
    }
    
    public void startServer(){
        this.serverBuilder = FusekiEmbeddedServer.create().setPort(3031);
         for(Map.Entry<String, Dataset> e : this.datasets.entrySet()){
            this.serverBuilder.add(e.getKey(), e.getValue());
        }
        this.server = this.serverBuilder.build();
        this.server.start() ;
        LOGGER.debug("Fuseki server started :) ");
    }
    
    public void closeConnection(){
        this.server.stop();
    }
}
