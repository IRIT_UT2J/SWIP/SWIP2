/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.irit.swip2.utils.SourceKB;

import java.io.IOException;
import org.apache.jena.query.ResultSet;

/**
 *
 * @author Fabien
 */
public class DistantSourceKB extends AbstractSourceKB{
    
    String sourceKBUrl;
    
    public DistantSourceKB(String projectName, String sourceKBUrl) throws IOException {
        super(projectName);
        this.sourceKBUrl = sourceKBUrl;
        this.startFusekiServer();
    }

    @Override
    public ResultSet selectQuery(String sparqlQuery) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public String getSourceKBUrl(){
        return this.sourceKBUrl;
    }
    
}
