/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.irit.swip2.utils.SourceKB;

import fr.irit.swip2.config.ReasonerType;
import fr.irit.swip2.utils.EmbeddedFuseki;
import java.io.IOException;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.DatasetFactory;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.text.EntityDefinition;
import org.apache.jena.query.text.TextDatasetFactory;
import org.apache.jena.query.text.TextIndex;
import org.apache.jena.query.text.TextIndexConfig;
import org.apache.jena.query.text.TextIndexLucene;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.reasoner.Reasoner;
import org.apache.jena.reasoner.ReasonerRegistry;
import org.apache.jena.sparql.core.DatasetGraph;
import org.apache.jena.sparql.core.DatasetGraphFactory;
import org.apache.jena.sparql.core.Quad;
import org.apache.jena.sparql.sse.SSE;
import org.apache.jena.tdb.TDB;
import org.apache.jena.util.FileManager;
import org.apache.jena.vocabulary.RDFS;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;

/**
 *
 * @author Fabien
 */
public class LocalSourceKB extends AbstractSourceKB {
    
    private static final Logger LOGGER = LogManager.getLogger(LocalSourceKB.class);
    
    private Model m;
    private Dataset dsToIndex;
    private Boolean indexURI;
    private ReasonerType reasonerType;
    
    
	public LocalSourceKB(String projectName, String baseFolderPath, ReasonerType reasonerType, Boolean indexURI) throws IOException {
        super(projectName);
        this.m = ModelFactory.createDefaultModel();
        this.indexURI = indexURI;
        this.reasonerType = reasonerType;
        /*LOGGER.info("Loading KB ("+projectName+") ...");
        Model schema = FileManager.get().loadModel("file:projects/"+projectName+"/model.owl");
        Model data = FileManager.get().loadModel("file:projects/"+projectName+"/data.owl");
        LOGGER.info("OK");
        Model infmodel;
        if(reasonerType != ReasonerType.NONE){
        	LOGGER.info("Reasonning in "+reasonerType.toString()+"... ");
        	infmodel = this.reason(reasonerType, schema, data);
        	LOGGER.info("Reasonning done. ");
        }
        else{
            LOGGER.info("Reasonning is skipped");
            infmodel = ModelFactory.createUnion(schema, data);
        }*/
        
        //DatasetGraph dsg = DatasetGraphFactory.createOneGraph(infmodel.getGraph()) ;
        /*Dataset ds1 = DatasetFactory.create(infmodel);
        Directory dir1 = new RAMDirectory() ;
        EntityDefinition eDef = new EntityDefinition("iri", "text");
        eDef.setPrimaryPredicate(RDFS.label);
        TextIndex tidx = new TextIndexLucene(dir1, new TextIndexConfig(eDef)) ;
        this.dsToIndex = TextDatasetFactory.create(ds1, tidx) ;
        this.dsToIndex.getContext().set(TDB.symUnionDefaultGraph, true) ;
        EmbeddedFuseki fusekiServer = EmbeddedFuseki.getFusekiServer();
        fusekiServer.addDataset(projectName, this.dsToIndex);*/
    }
	
	public void finalizeKB(){
        if(this.reasonerType != ReasonerType.NONE){
        	LOGGER.info("Reasonning in "+this.reasonerType.toString()+"... ");
        	this.m = this.reason(this.reasonerType, this.m);
        	LOGGER.info("Reasonning done. ");
        }
        else{
            LOGGER.info("Reasonning is skipped");
        }
        
        Dataset ds1 = DatasetFactory.create(this.m);
        Directory dir1 = new RAMDirectory() ;
        EntityDefinition eDef = new EntityDefinition("iri", "text");
        eDef.setPrimaryPredicate(RDFS.label);
        TextIndex tidx = new TextIndexLucene(dir1, new TextIndexConfig(eDef)) ;
        this.dsToIndex = TextDatasetFactory.create(ds1, tidx) ;
        this.dsToIndex.getContext().set(TDB.symUnionDefaultGraph, true) ;
        EmbeddedFuseki fusekiServer = EmbeddedFuseki.getFusekiServer();
        fusekiServer.addDataset(projectName, this.dsToIndex);
	}
	
	private Model reason(ReasonerType reasonerType, Model kb){
    	Reasoner reasoner;
    	switch(reasonerType){
    	case RDFS:
    		reasoner = ReasonerRegistry.getRDFSReasoner();
    		break;
    	case OWLMicro:
    		reasoner = ReasonerRegistry.getOWLMicroReasoner();
    		break;
    	case OWLMini:
    		reasoner = ReasonerRegistry.getOWLMiniReasoner();
    		break;
    	case OWL:
    		reasoner = ReasonerRegistry.getOWLReasoner();
    		break;
		default:
			LOGGER.warn("This default case should not be activated, defaulting to OWL reasoner");
			reasoner = ReasonerRegistry.getOWLReasoner();
    	}
    	return ModelFactory.createInfModel(reasoner, kb);
    }
    
    /*private Model reason(ReasonerType reasonerType, Model schema, Model data){
    	Reasoner reasoner;
    	switch(reasonerType){
    	case RDFS:
    		reasoner = ReasonerRegistry.getRDFSReasoner();
    		break;
    	case OWLMicro:
    		reasoner = ReasonerRegistry.getOWLMicroReasoner();
    		break;
    	case OWLMini:
    		reasoner = ReasonerRegistry.getOWLMiniReasoner();
    		break;
    	case OWL:
    		reasoner = ReasonerRegistry.getOWLReasoner();
    		break;
		default:
			LOGGER.warn("This default case should not be activated, defaulting to OWL reasoner");
			reasoner = ReasonerRegistry.getOWLReasoner();
    	}
    	reasoner = reasoner.bindSchema(schema);
    	
    	return ModelFactory.createInfModel(reasoner, data);
    }*/
    
    private void indexLabels(Dataset ds){
        String query = "PREFIX text: <http://jena.apache.org/text#>"+
                            "PREFIX rdfs:    <http://www.w3.org/2000/01/rdf-schema#>"+
                            "SELECT * "+
                            " WHERE {"+
                                "?a rdfs:label ?l."+
                            "}" ;
        QueryExecution qExec = QueryExecutionFactory.sparqlService(this.sparqlEndpointUrl, query);
        ResultSet execSelect = qExec.execSelect();
        while(execSelect.hasNext()){
            QuerySolution sol = execSelect.next();
            LOGGER.trace("RESULT : "+sol);
            String firstLabel = sol.get("l").asLiteral().getString();
            String l = firstLabel.replaceAll("_", " ").replaceAll("'", "\\\\'").trim();
            Quad quad;
            try{
                quad = SSE.parseQuad("(<index> <"+sol.get("a")+"> rdfs:label '"+l+"')") ;
                ds.asDatasetGraph().add(quad);
                LOGGER.trace("INDEX : "+sol.get("a")+" --> "+l);
            }
            catch(Exception e){
                LOGGER.error("INDEX ERROR /!\\ : "+sol.get("a")+" --> "+l);
                e.printStackTrace();
            }
        }
    }
    
    private void indexLabelsFromFragments(Dataset ds){
        int i = 0;
        while(i>= 0){
            String query = "PREFIX text: <http://jena.apache.org/text#>"+
                            "PREFIX rdfs:    <http://www.w3.org/2000/01/rdf-schema#>"+
                            "SELECT DISTINCT ?a "+
                            " WHERE {"+
                                "?a ?b ?c. "+
                                "FILTER NOT EXISTS {?a rdfs:label ?l}\n"+
                            "} \n" +
                            "ORDER BY ?a \n" +
                            "OFFSET   "+(i*10000)+"\n" +
                            "LIMIT  10000" ;
            QueryExecution qExec = QueryExecutionFactory.sparqlService(this.sparqlEndpointUrl, query);
            ResultSet execSelect = qExec.execSelect();
            int nb = 0;
            while(execSelect.hasNext()){
                nb ++;
                QuerySolution sol = execSelect.next();
                String uri = sol.get("a").toString();
                if(uri.contains("#")){
                    String fragment = uri.substring(uri.lastIndexOf("#")+1);
                    fragment = fragment.replaceAll("_", " ").replaceAll("([A-Z])", " $1").trim();
                    Quad quad = SSE.parseQuad("(<index> <"+sol.get("a").toString()+"> rdfs:label '"+fragment+"')") ;
                    LOGGER.trace("INDEX Fragments : "+sol.get("a")+" --> "+fragment);
                    ds.asDatasetGraph().add(quad);
                }
            }
            if(nb != 10000){
                i = -1;
            }
            else{
                i++;
            }
        }
    }
    
    public void loadLocalModel(String file){
    	LOGGER.info("Loading "+file+" into ("+projectName+") KB...");
    	Model localKB = FileManager.get().loadModel(file);
		this.m.add(localKB);
		LOGGER.info(file+" loaded.");
    }
    
    public void loadRemoteModel(String uri){
    	LOGGER.info("Loading "+uri+" into ("+projectName+") KB...");
    	Model remoteKB = ModelFactory.createDefaultModel();
    	remoteKB.read(uri);
		this.m.add(remoteKB);
		LOGGER.info(uri+" loaded.");
    }
    
    @Override
    public ResultSet selectQuery(String sparqlQuery){
    	LOGGER.debug("Source KB ("+this.sparqlEndpointUrl+") : "+sparqlQuery);
        QueryExecution qExec = QueryExecutionFactory.sparqlService(this.sparqlEndpointUrl, sparqlQuery);
        return qExec.execSelect();
    }
    
    @Override
    public void initSource(){
        LOGGER.info("Generate lucene index ... ");
        indexLabels(this.dsToIndex);
        // If in the config indexURI is not set, this condition will default to false
        if(this.indexURI != null && this.indexURI){
        	indexLabelsFromFragments(this.dsToIndex);
        }
    }
}
