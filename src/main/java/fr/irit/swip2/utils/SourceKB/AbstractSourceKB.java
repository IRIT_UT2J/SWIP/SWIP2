/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.irit.swip2.utils.SourceKB;


import fr.irit.swip2.SWIP;
import fr.irit.swip2.utils.EmbeddedFuseki;
import java.io.IOException;
import java.io.InputStream;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.DatasetFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.util.FileManager;
import org.apache.jena.util.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Fabien
 */
public abstract class AbstractSourceKB {
	
	private static final Logger LOGGER = LogManager.getLogger(AbstractSourceKB.class);
    
    protected String sparqlEndpointUrl;
    protected String projectName;
    //protected HashMap<String, Dataset> datasets;
    
    
//    private String queriesGraphUri;
//    private String patternsGraphUri;
    
    public AbstractSourceKB(String projectName) throws IOException{
        LOGGER.info("Parse patterns file ... ");
        this.projectName = projectName;
        String patterns = FileUtils.readWholeFileAsUTF8("projects/"+projectName+"/patterns.prdf");
        Dataset dataset = DatasetFactory.createTxnMem();
        //Model patternModel = PatternsTextToRdf.patternsTextToRdf(projectName, patterns, patterns, patterns);
        //this.patternsGraphUri = "http://swip.univ-tlse2.fr/ontologies/DATASET/"+projectName+"/patterns";
        //dataset.addNamedModel(this.patternsGraphUri, patternModel);
        
//        System.out.println("Prepare Query ontology...");
//        Model queriesModel = ModelFactory.createDefaultModel();
//        String inputFileName = "utils/SwipQueries.owl";
//        InputStream in = FileManager.get().open( inputFileName );
//        if (in == null) {
//            throw new IllegalArgumentException(
//                                         "File: " + inputFileName + " not found");
//        }
//        queriesModel.read(in, null);
//        this.queriesGraphUri = "http://swip.univ-tlse2.fr/ontologies/DATASET/"+projectName+"/queries";
//        dataset.addNamedModel(this.queriesGraphUri, queriesModel);
//        EmbeddedFuseki fusekiServer = EmbeddedFuseki.getFusekiServer();
//        fusekiServer.addDataset("/"+projectName+"_SWIP", dataset);
        
        this.sparqlEndpointUrl = "http://localhost:3031/"+projectName;
    }
   
    public void startFusekiServer(){
       
    }
    
    //TODO : use this method instead of HTTP request in the whole project
    public abstract ResultSet selectQuery(String sparqlQuery);
        
    public String getSourceKBUrl(){
        return this.sparqlEndpointUrl;
    }
   
    
//    public String getPatternsGraphUri(){
//        return this.patternsGraphUri;
//    }
//    public String getQueriesGraphUri(){
//        return this.queriesGraphUri;
//    }
    
    public void initSource(){
        
    }
}
