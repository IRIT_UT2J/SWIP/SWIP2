/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.irit.swip2.nltopivot.pivotRules;

import edu.stanford.nlp.util.Pair;
import fr.irit.swip2.SWIPWorkflow;
import fr.irit.swip2.nltopivot.parser.model.DependencyTree;
import fr.irit.swip2.nltopivot.parser.model.MyDependencyNode;
import fr.irit.swip2.nltopivot.parser.model.MyEdge;
import fr.irit.swip2.utils.OpenQASwipParameters;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.aksw.openqa.Properties;
import org.aksw.openqa.component.answerformulation.AbstractQueryParser;
import org.aksw.openqa.component.context.IContext;
import org.aksw.openqa.component.param.IParamMap;
import org.aksw.openqa.component.param.IResultMap;
import org.aksw.openqa.component.providers.impl.ServiceProvider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Fabien
 */
public class PivotGenerator extends AbstractQueryParser{
	
	private static final Logger LOGGER = LogManager.getLogger(SWIPWorkflow.class);
    
     public PivotGenerator(Map<String, Object> params) {
        super(params);
    }
     
     public PivotGenerator(){
         super(null);
     }
    
     @Override
    public boolean canProcess(IParamMap param) {
        return param.contains(Properties.Literal.TEXT);
    }
    
    @Override
    public String getVersion() {
            return "SWIP2-PivotGen"; // version
    }

    @Override
    public String getLabel(){
        return "Rules";
    }

    @Override
    public String getId() {
            return getLabel() + " " + getVersion(); // id = label + version
    }

    @Override
    public List<? extends IResultMap> process(IParamMap param, ServiceProvider sp, IContext ic) {       

    	LOGGER.info("Generate pivot query from dependencies tree ...");
        
        DependencyTree depTree = param.getParam(OpenQASwipParameters.DEP_GRAPH, DependencyTree.class);
        nerMap = param.getParam(OpenQASwipParameters.NER_MAP, HashMap.class);
        MyDependencyNode headNode = param.getParam(OpenQASwipParameters.HEAD_NODE, MyDependencyNode.class);
        
        String pivotQuery = ""; // will contain the generated pivot query
        if(depTree.getDependencyNodes().length == 1){
            pivotQuery = headNode.getLemma();
        }
        else{
            pivotQuery = this.dependenciesToPivot(depTree, "en");
        }
        
        
        LOGGER.info("------**---**-----**** PIVOT QUERY : "+pivotQuery);
        param.setParam(OpenQASwipParameters.PIVOT_QUERY, pivotQuery);
        return null;
        
    }
    
    
    HashMap<Pair<Integer, Integer>, String> nerMap;
    
    boolean ASK = false; // ASK SPARQL query
    boolean COUNT = false; // COUNT in  SELECT SPARQL query
    boolean DESCRIBE = false; // DESCRIBE SPARQL query
    boolean Q2_SPECIFIQUE = false; //?
    Set<Integer> visitedNodes = null; //id des noeuds de l'arbre déjà visité donc pas besoin de les rajouter à la pivot
    Set<MyDependencyNode> visitedNodes2 = null; // noeuds de l'arbre déjà visité donc pas besoin de les rajouter à la pivot
    String pivotQuery = null;
    String nextElementRole = null; //role attendu pour l'élement suivany
    String subjectOfNextQ2 = null; // cas où on doit rajouter un q2 pour un terme qui était déjà dans la pivot car on lui trouve un complèement
    boolean inverseQ3 = false; //???
    String queryObject = null; //focus of the query
    MyDependencyNode firstNodeToBrowse = null;
    List<String> ignoredRelList = new ArrayList<>(Arrays.asList(new String[]{"prep", "det", "punct", "auxpass", "case"}));
    List<String> ignoredRoots = new ArrayList<>(Arrays.asList(new String[]{"?"}));
    final List<String> ignoredPostags = new ArrayList<>(Arrays.asList(new String[]{"DET", "WRB", "WDT", "IN", "IN/that", "WP", "PP", "PP$", "PRP", "PRP$", "DT", "SENT", "RB", "RBR", "JJR", "VHP", "VHZ","TO"}));
    final List<String> ignoredLemmas = new ArrayList<>(Arrays.asList(new String[]{"do", "be", "show", "give", "I", "list", "all", "many", "have", "which"}));
    final List<String> keepLemmas = new ArrayList<>(Arrays.asList(new String[]{"outside"}));//ignored because of its postag but we need to keepit

    
    
    
    
    
    public String dependenciesToPivot(DependencyTree dt, String lang) {
        //logger.info("----------------------------------");
        //logger.info("+ Interpreting dependency graph...");
        queryObject = null; // focus of the query
        visitedNodes2 = new HashSet<>();
        ASK = false;
        COUNT = false;
        DESCRIBE = false;
        pivotQuery = "";
        nextElementRole = "e1q3"; //type de sub query qui va être cherché a priori
        inverseQ3 = false; //???

       /********* determine if the considered query implies to add a new keyword according to the interrogation word ***/
        MyDependencyNode firstTokenInSentence = dt.getDependencyNode(0); //LOGGER.info("premier mot de la phrase" +firstTokenInSentence.getLemma());   
        MyDependencyNode secondTokenInSentence = dt.getDependencyNode(1); //LOGGER.info("deuxième mot de la phrase" +secondTokenInSentence.getLemma());
        
        String firstTokenLemma = firstTokenInSentence.getLemma();
        String secondTokenLemma = secondTokenInSentence.getLemma();
        if (firstTokenLemma.equalsIgnoreCase("who")) {
            firstNodeToBrowse = firstTokenInSentence;
            pivotQuery += "person";
            queryObject = "person";
            nextElementRole = "e2q3";
        } else if (firstTokenLemma.equalsIgnoreCase("when")) {
            firstNodeToBrowse = firstTokenInSentence;
            pivotQuery += "date<?>";
            queryObject = "date<?>";
            nextElementRole = "e2q3";
            inverseQ3 = true; // pour inverser le e1 avec le e3: que pour la date?
        }
        else if (firstTokenLemma.equalsIgnoreCase("Where")){ //
            firstNodeToBrowse = firstTokenInSentence;
            pivotQuery += "location";
            queryObject = "location";
            Q2_SPECIFIQUE = true;
            nextElementRole = "e2q2"; 
        }
        if (secondTokenLemma.equalsIgnoreCase("long") && firstTokenLemma.equalsIgnoreCase("how")) {
            LOGGER.info("searching for a duration query");
            Q2_SPECIFIQUE=true;
            MyDependencyNode verbe = dt.getDependencyNode(secondTokenInSentence.getHeadEdge().getHeadNodeId());
            queryObject = "duration";
            pivotQuery += "duration";
            nextElementRole = "e2q2";
            subjectOfNextQ2 = "duration";
            firstNodeToBrowse = verbe;
            visitedNodes2.add(secondTokenInSentence);
        }
        
        /** first version to handle count queries **/
        // else if (firstTokenLemma.equalsIgnoreCase("How") && secondTokenLemma != null && secondTokenLemma.equalsIgnoreCase("many")) {
         //   COUNT = true;
         //   firstNodeToBrowse = firstTokenInSentence;
            /*ajouter query object*/
        //} 
        
        else {
            // look for query object
            searchQueryObject(dt);
            if (queryObject == null) {
               // searchQueryObjectAccordingToWordsOrder(dt);
                firstNodeToBrowse = dt.getDependencyNode(dt.getSentenceHeadId());
                if (queryObject == null) {
                    ASK = true;
                }

            }
        }
        // browse dependency tree
        LOGGER.debug("====>focus "+queryObject+ "now generating pivot query a priori du type "+nextElementRole);
        browseNode(dt, firstNodeToBrowse);
        
        pivotQuery = pivotQuery.trim();
        if(pivotQuery.endsWith("=")){
            pivotQuery = pivotQuery.substring(0, pivotQuery.lastIndexOf("="));
        }
      //  pivotQuery += ".";
        if (COUNT) {
            pivotQuery += " COUNT.";
        } else if (ASK) {
            pivotQuery += " ASK.";
        } else if (DESCRIBE) {
            pivotQuery += ". DESCRIBE";
        } 
       
            else {
              if (pivotQuery.contains(queryObject)) {
                  pivotQuery = pivotQuery.replaceAll(queryObject, "?" + queryObject);
              
              } else {
                  pivotQuery += "?" + queryObject + ".";
              }
        }

        return pivotQuery;
    }

    private void searchQueryObject(DependencyTree dt) {
        //looks for the focus of the query
        LOGGER.info("searching query focus");
        
        MyDependencyNode firstTokenInSentence = dt.getDependencyNode(0); //LOGGER.info("premier mot de la phrase" +firstTokenInSentence.getLemma());   
        MyDependencyNode secondTokenInSentence = dt.getDependencyNode(1);
     
        //dealing with ''what/which is/are'' questions
        if ((firstTokenInSentence.getLemma().equalsIgnoreCase("What") || firstTokenInSentence.getLemma().equalsIgnoreCase("Which"))&& (firstTokenInSentence.getPostag().equalsIgnoreCase("WP")||firstTokenInSentence.getPostag().equalsIgnoreCase("WDT")) &&secondTokenInSentence.getLemma()!= null && secondTokenInSentence.getLemma().equalsIgnoreCase("be")){ 
                    LOGGER.info("searching for a what/which is/are query");
                    firstNodeToBrowse = firstTokenInSentence;
                    //searching for nsubj edge for queryObject ie questions of the type what is UTA? or what are the sensors ... 
                    for (MyEdge edge : firstTokenInSentence.getRightDependentEdges()){
                    if (edge.getDeprel().equalsIgnoreCase("nsubj")) { // a noun is in depedence with the what
                        queryObject=dt.getDependencyNode(edge.getDependentNodeId()).getLemma();   
                        LOGGER.info("query objet "+queryObject);
                        return;
                        }
                    }
                    //case which/what are WDT WH-determiner, dobj of be (secondTokenInSentence), thus looking for nsubj of be (secondTokenInSentence)
                    for (MyEdge edge : secondTokenInSentence.getRightDependentEdges()){
                    if (edge.getDeprel().equalsIgnoreCase("nsubj")) { // a noun is in depedence with the what
                        queryObject=dt.getDependencyNode(edge.getDependentNodeId()).getLemma();   
                        LOGGER.info("query objet "+queryObject);
                        return;
                        }
                    }
                    
                    
                }
                
        // dealing with ''which NOUN verbe'' queries   
        if ((firstTokenInSentence.getLemma().equalsIgnoreCase("which")||firstTokenInSentence.getLemma().equalsIgnoreCase("What")) && firstTokenInSentence.getPostag().equalsIgnoreCase("WDT") && firstTokenInSentence.getHeadEdge().getDeprel().equalsIgnoreCase("det")) {
                    LOGGER.info("searching for a which query");
                     
                    firstNodeToBrowse = firstTokenInSentence;
                    queryObject = dt.getDependencyNode(firstTokenInSentence.getHeadEdge().getHeadNodeId()).getLemma();//this.getNERElement(head);
                    //logger.info("query object : which me"+node.getLemma() +"--- first node"+firstNodeToBrowse.getLemma() );
                    return;
                }
        // dealing with ''give|list [me][all][the list of] noun'' queries
        if (firstTokenInSentence.getLemma().equalsIgnoreCase("give") || firstTokenInSentence.getLemma().equalsIgnoreCase("list")){// && firstTokenInSentence.getPostag().equalsIgnoreCase("WDT") && firstTokenInSentence.getHeadEdge().getDeprel().equalsIgnoreCase("det")) {
                    LOGGER.info("searching for a give query");
                     
                    for (MyEdge edge : firstTokenInSentence.getRightDependentEdges()){
                    if (edge.getDeprel().equalsIgnoreCase("dobj")) { // a noun is dobj of the give
                        MyDependencyNode nounNode = dt.getDependencyNode(edge.getDependentNodeId());
                        if (nounNode.getLemma().equalsIgnoreCase("list")){
                            for (MyEdge edgeList : nounNode.getRightDependentEdges()){
                                if (edgeList.getDeprel().equals("nmod")) queryObject = dt.getDependencyNode(edgeList.getDependentNodeId()).getLemma();
                                visitedNodes2.add(nounNode);
                            }
                        }
                        else queryObject = nounNode.getLemma();  
                      
                        LOGGER.info("query objet "+queryObject);
                        firstNodeToBrowse = nounNode;
                 
                        //nextElementRole = "e1q2";//we can only find a noun complement of the queryobject TODO : propo relative
                        return;
                        }
                    }
                   
                }
        
    }    
                
    
    
    private String getNERElement(MyDependencyNode node){
        String ret = node.getLemma();
//        boolean founded = false;
//        for(Pair<Integer, Integer> position : nerMap.keySet()){
//            if(node.getBeginPos() >= position.first && node.getEndPos() <= position.second){
//                ret = nerMap.get(position);
//                break;
//            }
//        }
        
        return ret;
    }
    
    private void browseNode(DependencyTree dt, MyDependencyNode node) {
        //logger.info("browsing " + node);
        // process token
        String headEdgeDeprel = "";
        String headLemma = "";
        String headPostag = "";
        String currentRole = "";
        LOGGER.debug("BROWSE NODE : ");
        LOGGER.debug(node);
        

        int headId = node.getHeadEdge().getHeadNodeId(); // on récupère le père
   
        if (headId >= 0) {
            headEdgeDeprel = node.getHeadEdge().getDeprel(); // on récupère la dépendance avec le père
            headLemma = this.getNERElement(dt.getDependencyNode(headId));
             LOGGER.debug("analyse dependancy "+node.getLemma()+"-"+headLemma);
            headPostag = dt.getDependencyNode(headId).getPostag();
        }
        if (keepLemmas.contains(node.getLemma()) || (!ignoredPostags.contains(node.getPostag()) && !ignoredLemmas.contains(node.getLemma()))) { // si ce n'est pas un mot vide
           
                if (((headEdgeDeprel.equalsIgnoreCase("nsubj") || headEdgeDeprel.equalsIgnoreCase("compound") ||  headEdgeDeprel.equalsIgnoreCase("nn") || headEdgeDeprel.equalsIgnoreCase("amod")))//
                        &&(! (headPostag.equalsIgnoreCase("VVP") ||headPostag.equalsIgnoreCase("VVD") ||headPostag.equalsIgnoreCase("VV")) ) 
                        && pivotQuery.contains(headLemma) ) {
                    LOGGER.info("token is maybe the type of its head...  and has lead to the creation of a Q2 -"+headLemma+" -:- "+ this.getNERElement(node));
                    currentRole=nextElementRole; // on tombe sur un Q2 alors qu'on est au milieu d'un traitement de Q3
                    nextElementRole = "e2q2";
                    subjectOfNextQ2 = headLemma;
                    addElementToPivotQuery(this.getNERElement(node));
                    nextElementRole=currentRole;
         
                }
               /* if (headEdgeDeprel.equalsIgnoreCase("nmod") && !nextElementRole.contains("e3q3") 
                        && pivotQuery.contains(headLemma) && !pivotQuery.contains(node.getLemma())) {
                    LOGGER.debug("found a noun modifier and have already taken into accournt the noun, thus adding a Q2");
                    subjectOfNextQ2 = headLemma; 
                    nextElementRole = "e2q2";
                    addElementToPivotQuery(this.getNERElement(node));
                    
                }*/
                //else if (headEdgeDeprel.equalsIgnoreCase("nmod") && !nextElementRole.contains("e3q3") && !pivotQuery.contains(headLemma))
                
                else addElementToPivotQuery(this.getNERElement(node));
            
        } else {
            LOGGER.info("* dependency "+node.getLemma() +"-"+dt.getDependencyNode(headId).getLemma()+" ignored because not relevant");
        }
        visitedNodes2.add(node);
        //}
        //else LOGGER.info("* dependency "+node.getLemma() +"-"+dt.getDependencyNode(headId).getLemma()+"ignored because "+dt.getDependencyNode(headId).getLemma()+" already visited");
        // process children
        for (MyEdge childEdge : node.getLeftDependentEdges()) {
            MyDependencyNode candidateNode = dt.getDependencyNode(childEdge.getDependentNodeId());
            if (!visitedNodes2.contains(candidateNode)) {
                browseNode(dt, candidateNode);
            }
        }
        for (MyEdge childEdge : node.getRightDependentEdges()) {
            MyDependencyNode candidateNode = dt.getDependencyNode(childEdge.getDependentNodeId());
            if (!visitedNodes2.contains(candidateNode)) {
                browseNode(dt, candidateNode);
            }
        }
        // process head
        if (headId >= 0) {
            MyDependencyNode candidateNode = dt.getDependencyNode(headId);
            if (!visitedNodes2.contains(candidateNode)) {
                browseNode(dt, candidateNode);
            }
        }
    }

    private void addElementToPivotQuery(String element) {
        LOGGER.debug("CONSTRUCTING PIVOT QUERY -- ADDING A : "+nextElementRole+" with "+element+" to "+pivotQuery);
        //logger.info("**pivot avant "+pivotQuery+" ajout de "+element+" en tant que "+nextElementRole);
        if(ASK){
            inverseQ3 = true;
        }
        if (inverseQ3) {
            LOGGER.debug("**inverse");
            if (nextElementRole.equalsIgnoreCase("e2q3")) {
                pivotQuery = element + ": " + pivotQuery;
                nextElementRole = "e1q3";
            } else if (nextElementRole.equalsIgnoreCase("e1q3")) {
                pivotQuery = pivotQuery.replaceFirst(":", "=");
                pivotQuery = element + ": " + pivotQuery;
                nextElementRole = "e3q3";
                subjectOfNextQ2 = element;
            }
            else if (nextElementRole.equalsIgnoreCase("e3q3")) {
                pivotQuery = pivotQuery+element;
                nextElementRole = "e2q2";
                subjectOfNextQ2 = element;
            }
            else if (nextElementRole.equalsIgnoreCase("e2q2")) {
               pivotQuery += ". "+subjectOfNextQ2 + ": " + element;
                nextElementRole = "e1q3";
            }
            
        } else {
            LOGGER.debug("**not inverse");
            if (nextElementRole.equalsIgnoreCase("e1q3")) {
                if (!pivotQuery.isEmpty()) pivotQuery +=".";
                pivotQuery += element;
                nextElementRole = "e2q3";
            } else if (nextElementRole.equalsIgnoreCase("e2q3")) {
                pivotQuery += ": " + element;
                nextElementRole = "e3q3";
            } else if (nextElementRole.equalsIgnoreCase("e3q3")) {
                pivotQuery += "= " + element;
                nextElementRole = "e1q3";//"e2q2";
                subjectOfNextQ2 = element;
            } else if (nextElementRole.equalsIgnoreCase("e2q2")) {
                
                if (Q2_SPECIFIQUE) { // Q2 is induce by the begining of the tree
                    Q2_SPECIFIQUE=false;
                    subjectOfNextQ2=element;
                    pivotQuery =  pivotQuery + ": " + element + ". ";
                }
                else pivotQuery =  subjectOfNextQ2 + ": " + element + ". "+pivotQuery;
                nextElementRole = "e1q3";
            }
        }
        //logger.info("**pivot après "+pivotQuery+" nextElementRole "+nextElementRole+"subjectOfNextQ2 "+subjectOfNextQ2+" inverse "+inverseQ3 );
        LOGGER.debug("RESULT QUERY : "+pivotQuery +"maintenant à la recherche de "+nextElementRole);
    }
    
    
    
    
    
    
    private MyDependencyNode getFocus(DependencyTree dt, String lang){
        // va avec dependenciesToPivot_BACK pas utilisé
        // will identify what is the focus of the query (ie projection attribut in the sparql query, ie keyword prefixed with a ? in the pivot query 
        MyDependencyNode focusNode = null;
        
        HashMap<String, MyDependencyNode> nodeFromLemma = new HashMap<>();
        for(MyDependencyNode n : dt.getDependencyNodes()){
            nodeFromLemma.put(n.getLemma(), n);
        }
        
        MyDependencyNode firstTokenInSentence = dt.getDependencyNode(0);
        MyDependencyNode secondTokenInSentence = dt.getDependencyNode(1);
        String firstTokenLemma = firstTokenInSentence.getLemma();
        String secondTokenLemma = secondTokenInSentence.getLemma();
        
        
        if(firstTokenLemma.equalsIgnoreCase("List")){
            focusNode = secondTokenInSentence;
        }
        else {
            // look for query object
            searchQueryObject(dt);
            if (queryObject == null) {
             //   searchQueryObjectAccordingToWordsOrder(dt);
                if (queryObject == null) {
                    ASK = true;
                }
            }
            if(queryObject != null)
                focusNode = nodeFromLemma.get(queryObject);
               
        }
        LOGGER.info("focus à la fin de getFocus "+focusNode.getLemma());
        return focusNode;
    }
    
    
    
    
    public String dependenciesToPivot_BACK(DependencyTree dt, String lang) {
        //pas utilisé
        String ret = "";
        
        MyDependencyNode focusNode = this.getFocus(dt, lang);
        
        HashMap<MyDependencyNode, MyDependencyNode> subjFromVerb = new HashMap<>();
        HashMap<Integer, MyDependencyNode> nodeIncluded = new HashMap<>();
        for(MyDependencyNode mdn : dt.getDependencyNodes()){
            if(mdn.getPostag().equalsIgnoreCase("VBD") && !ignoredLemmas.contains(mdn.getLemma())){
                MyDependencyNode subj = null;
                MyDependencyNode dobj = null;
                ArrayList<MyEdge> dependencies = new ArrayList<MyEdge>();
                dependencies.addAll(Arrays.asList(mdn.getLeftDependentEdges()));
                dependencies.addAll(Arrays.asList(mdn.getRightDependentEdges()));
                boolean founded = false;
                if(dependencies.size() > 0){
                    int i = 0;
                    while(i < dependencies.size() && !founded){
                        MyEdge e = dependencies.get(i);
                        if(e.getDeprel().equalsIgnoreCase("nsubj")){
                            MyDependencyNode en = dt.getDependencyNode(e.getDependentNodeId());
                            if(!ignoredLemmas.contains(en.getLemma())){
                                subj = en;
                            }
                        }
                        else if(e.getDeprel().equalsIgnoreCase("dobj")){
                            MyDependencyNode en = dt.getDependencyNode(e.getDependentNodeId());
                            if(!ignoredLemmas.contains(en.getLemma())){
                                dobj = en;
                            }
                        }
                        i++;
                        founded = (subj != null && dobj != null);
                    }
                }
                if(founded){
                    subjFromVerb.put(mdn, subj);
                    ret += subj.getLemma()+":"+mdn.getLemma()+"="+dobj.getLemma()+". ";
                    nodeIncluded.put(subj.getId(), subj);
                    nodeIncluded.put(mdn.getId(), mdn);
                    nodeIncluded.put(dobj.getId(), dobj);
                }
            }
            else if(mdn.getPostag().equalsIgnoreCase("NN") && !ignoredLemmas.contains(mdn.getLemma())){
                ArrayList<MyEdge> dependencies = new ArrayList<MyEdge>();
                dependencies.addAll(Arrays.asList(mdn.getLeftDependentEdges()));
                dependencies.addAll(Arrays.asList(mdn.getRightDependentEdges()));
                int i = 0;
                while(i < dependencies.size()){
                    MyEdge e = dependencies.get(i);
                    if(!ignoredRelList.contains(e.getDeprel())){
                        MyDependencyNode en = dt.getDependencyNode(e.getDependentNodeId());
                        if(!ignoredLemmas.contains(en.getLemma())){
                            ret += mdn.getLemma()+":"+en.getLemma()+". ";
                            nodeIncluded.put(mdn.getId(), mdn);
                            nodeIncluded.put(en.getId(), en);
                        }
                    }
                    i++;
                }
            }
        }
        for(MyDependencyNode mdn : dt.getDependencyNodes()){
            if(!ignoredLemmas.contains(mdn.getLemma()) && !ignoredPostags.contains(mdn.getPostag()) && !nodeIncluded.containsKey(mdn.getId())){
                ret += mdn.getLemma()+". ";
            }
        }
        
        LOGGER.debug("PIVOT QUERY TEST : "+ret);
        if(focusNode != null){
            ret = ret.replaceAll(focusNode.getLemma(), "?"+focusNode.getLemma());
        }
        else{
            ret = ret+" ASK.";
        }
        LOGGER.debug("With focus : "+ret);
        return ret;
    }
    
    
    
    
    
    
    
    
}
