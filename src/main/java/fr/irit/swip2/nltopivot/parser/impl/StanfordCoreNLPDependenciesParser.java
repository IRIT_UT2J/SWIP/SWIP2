/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.irit.swip2.nltopivot.parser.impl;

import edu.stanford.nlp.ie.util.RelationTriple;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreAnnotations.AnswerAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.NamedEntityTagAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.naturalli.NaturalLogicAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.PennTreebankLanguagePack;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.stanford.nlp.trees.TypedDependency;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.util.Pair;
import fr.irit.swip2.SWIPWorkflow;
import fr.irit.swip2.nltopivot.parser.AbstractDependenciesParser;
import fr.irit.swip2.nltopivot.parser.model.DependencyTree;
import fr.irit.swip2.nltopivot.parser.model.MyDependencyNode;
import fr.irit.swip2.nltopivot.parser.model.MyEdge;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Fabien
 */
public class StanfordCoreNLPDependenciesParser extends AbstractDependenciesParser{

	private static final Logger LOGGER = LogManager.getLogger(StanfordCoreNLPDependenciesParser.class);
    private HashMap<Pair<Integer, Integer>, String> nerMap;
    
    public StanfordCoreNLPDependenciesParser(Map<String, Object> params) {
        super(params);
    }
    
    public StanfordCoreNLPDependenciesParser() {
        super(null);
    }

    @Override
    public String getLabel() {
        return "StanfordCoreNLP";
    }

    
    //Needed to get spatial and time NER
    private HashMap<Pair<Integer, Integer>, String> getNeredQuery(String query, CoreMap sentence){
        
        HashMap<Pair<Integer, Integer>, String> ret = new HashMap<>();
        String curNER = null;
        String replaceNER = null;
        int begin = -1;
        int end = -1;
        CoreLabel lastToken = null;
        
        for (CoreLabel token: sentence.get(CoreAnnotations.TokensAnnotation.class)) {
            lastToken = token;
            String word = token.get(CoreAnnotations.TextAnnotation.class);
            String pos = token.get(CoreAnnotations.PartOfSpeechAnnotation.class);
            String lemma = token.get(CoreAnnotations.LemmaAnnotation.class);
            String ner = token.get(NamedEntityTagAnnotation.class);
                if(curNER == null){
                    if(!ner.equalsIgnoreCase("O")){
                        begin = token.beginPosition();
                        end = token.endPosition();
                        curNER = ner;
                        replaceNER = word;
                    }
                }
                else if(curNER != ner){
                    //newQuery = newQuery.substring(0, begin)+(replaceNER.replaceAll(" ", "_"))+newQuery.substring(end);
                    ret.put(new Pair(begin, end), replaceNER.replaceAll(" ", "_"));
                    end = -1;
                    if(!ner.equalsIgnoreCase("O")){
                        begin = token.beginPosition();
                        end = token.endPosition();
                        curNER = ner;
                        replaceNER = word;
                        
                    }
                    else{
                        begin = -1;
                        curNER = null;
                        replaceNER = null;
                    }
                }
                else{
                    replaceNER += " "+word;
                    end = token.endPosition();
                }
        }
        if(curNER != null && lastToken != null){
            ret.put(new Pair(begin, lastToken.endPosition()), replaceNER.replaceAll(" ", "_"));
        }
        LOGGER.debug("TEST NER QUERY : "+query+" --> ");
        LOGGER.debug(ret);
        return ret;
    }
    
    @Override
    public DependencyTree getDependenciesGraph(String nerQuery) {
        MyDependencyNode.resetCurId();
        
        
        java.util.Properties props = new java.util.Properties();
        props.setProperty("annotators","tokenize, ssplit, pos, lemma, parse, depparse, ner");
        StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
        String text = nerQuery;
        Annotation annotation = new Annotation(text);
        pipeline.annotate(annotation);
        List<CoreMap> sentences = annotation.get(CoreAnnotations.SentencesAnnotation.class);
        CoreMap sentence = sentences.get(0); // get first sentence because there is onlye one for a question
        
        //this.nerMap = this.getNeredQuery(nerQuery, sentence);
        
        
        Tree tree = sentence.get(TreeCoreAnnotations.TreeAnnotation.class);
        
        // Get dependency tree
        TreebankLanguagePack tlp = new PennTreebankLanguagePack();
        GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();
        GrammaticalStructure gs = gsf.newGrammaticalStructure(tree);
        Collection<TypedDependency> td = gs.typedDependenciesCollapsed();
   
        Object[] list = td.toArray();
        TypedDependency typedDependency;
        HashMap<String, MyDependencyNode> nodesMap = new HashMap<>();
        MyDependencyNode headNode = null;
        for (CoreLabel token: sentence.get(CoreAnnotations.TokensAnnotation.class)) {
            String word = token.get(CoreAnnotations.TextAnnotation.class);
            String pos = token.get(CoreAnnotations.PartOfSpeechAnnotation.class);
            String lemma = token.get(CoreAnnotations.LemmaAnnotation.class);
            String ner = token.get(NamedEntityTagAnnotation.class);
            if(!pos.equals(".")){
                MyDependencyNode node = new MyDependencyNode(word, lemma, pos, token.beginPosition(), token.endPosition());
                nodesMap.put(word, node);
            }
        }
        for (Object object : list) {
            typedDependency = (TypedDependency) object;
            IndexedWord gov = typedDependency.gov();
            IndexedWord dep = typedDependency.dep();
            //System.out.println("Depdency  : "+gov.word()+"("+gov.tag()+" :: "+gov.lemma()+") --"+typedDependency.reln().getShortName()+"--> "+dep.word()+"("+dep.tag()+" :: "+dep.lemma()+")");
            MyDependencyNode govNode = null;
            MyDependencyNode depNode = null;
            if(gov.word() != null){
                if(!nodesMap.containsKey(gov.word())){
                    govNode = new MyDependencyNode(gov.word(), gov.lemma(), gov.tag(), gov.beginPosition(), gov.endPosition());
                    nodesMap.put(gov.word(), govNode);
                }
                else{
                    govNode = nodesMap.get(gov.word());
                }
            }
            if(!nodesMap.containsKey(dep.word())){
                depNode = new MyDependencyNode(dep.word(), dep.lemma(), dep.tag(), dep.beginPosition(), dep.endPosition());
                nodesMap.put(dep.word(), depNode);
            }
            else{
                depNode = nodesMap.get(dep.word());
            }
            if(govNode != null){
                 MyEdge dependency = new MyEdge(govNode.getId(), depNode.getId(), typedDependency.reln().getShortName());
                if(gov.beginPosition() < dep.beginPosition()){
                    govNode.addRightDependentEdge(dependency);
                }
                else{
                    govNode.addLeftDependentEdge(dependency);
                }
                depNode.setHeadEdge(dependency);
            }
            else if(typedDependency.reln().getShortName().equals("root")){
                headNode = depNode;
                headNode.setHeadEdge(new MyEdge(-1, headNode.getId(), "root"));
            }
        }
        DependencyTree dt = new DependencyTree(headNode.getId(), nodesMap.values().toArray(new MyDependencyNode[nodesMap.values().size()]));
//        System.out.println("COUCOU TEST PARSER : ");
//        System.out.println("TEST FROM MAP : ");
//        MyDependencyNode[] toArray = nodesMap.values().toArray(new MyDependencyNode[nodesMap.values().size()]);
//        for(MyDependencyNode n : toArray){
//            System.out.println("NODE : "+n.getId());
//            System.out.println(n);
//        }
//        System.out.println("HEAD : ");
//        System.out.println(headNode);
        return dt;
    }
    
    public HashMap<Pair<Integer, Integer>, String> getNerMap(){
        return this.nerMap;
    }
    
}
