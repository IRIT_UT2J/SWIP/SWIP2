package fr.irit.swip2.nltopivot.parser.model;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MyDependencyNode implements Comparable<MyDependencyNode>{

    private static int curId = 0;
    public static void resetCurId(){
        curId = 0;
    }
    
    int id;
    String form = null;
    String lemma = null;
    String postag = null;
    MyEdge headEdge = null;
    ArrayList<MyEdge> leftDependentEdges = null;
    ArrayList<MyEdge> rightDependentEdges = null;
    int beginPos;
    int endPos;

    public MyDependencyNode() {
    }

    public MyDependencyNode(String json) throws ParseException, JSONException {
        JSONObject jo = new JSONObject(json);
        this.form = jo.getString("form");
        this.lemma = jo.getString("lemma");
        this.postag = jo.getString("postag");
        this.headEdge = new MyEdge(jo.getString("headEdge"));
        JSONArray array = jo.getJSONArray("leftDependentEdges");
        int length = array.length();
        //this.leftDependentEdges = new MyEdge[length];
        this.leftDependentEdges = new ArrayList<>();
        for (int i=0; i<length; i++) {
            //this.leftDependentEdges[i] = new MyEdge(array.getString(i));
            this.leftDependentEdges.add(new MyEdge(array.getString(i)));
        }
        array = jo.getJSONArray("rightDependentEdges");
        //length = array.length();
        this.rightDependentEdges = new ArrayList<>();
        //this.rightDependentEdges = new MyEdge[length];
        for (int i=0; i<length; i++) {
            //this.rightDependentEdges[i] = new MyEdge(array.getString(i));
            this.rightDependentEdges.add(new MyEdge(array.getString(i)));
        }
    }

    public MyDependencyNode(String form, String lemma, String postag, int beginPos, int endPos) {
        this.id = curId;
        curId ++;
        this.form = form;
        this.lemma = lemma;
        this.postag = postag;
        this.leftDependentEdges = new ArrayList<>();
        this.rightDependentEdges = new ArrayList<>();
        this.beginPos = beginPos;
        this.endPos = endPos;
    }
    
    public int getId(){
        return this.id;
    }

    public MyEdge[] getLeftDependentEdges() {
        return leftDependentEdges.toArray(new MyEdge[this.leftDependentEdges.size()]);
    }

    public void setLeftDependentEdges(MyEdge[] leftDependentEdges) {
        this.leftDependentEdges = new ArrayList<>(Arrays.asList(leftDependentEdges));
    }
    
    public void addLeftDependentEdge(MyEdge leftDependentEdge){
        this.leftDependentEdges.add(leftDependentEdge);
    }

    public MyEdge[] getRightDependentEdges() {
        return rightDependentEdges.toArray(new MyEdge[this.rightDependentEdges.size()]);
    }

    public void setRightDependentEdges(MyEdge[] rightDependentEdges) {
        this.rightDependentEdges = new ArrayList<>(Arrays.asList(rightDependentEdges));
    }
    
    public void addRightDependentEdge(MyEdge rightDependentEdge){
        this.rightDependentEdges.add(rightDependentEdge);
    }

    public String getForm() {
        return form;
    }

    public void setForm(String form) {
        this.form = form;
    }

    public MyEdge getHeadEdge() {
        return headEdge;
    }

    public void setHeadEdge(MyEdge headEdge) {
        this.headEdge = headEdge;
    }

    public String getLemma() {
        return lemma;
    }

    public void setLemma(String lemma) {
        this.lemma = lemma;
    }

    public String getPostag() {
        return postag;
    }

    public void setPostag(String postag) {
        this.postag = postag;
    }

    @Override
    public String toString() {
        String result = "MyDependencyNode ("+this.id+") "+this.beginPos+" - "+this.endPos+" :\n\t\t\t- form=" + form + ", lemma=" + lemma + ", postag=" + postag;
        result += "\n\t\t\t- headEdge: " + headEdge;
        result += "\n\t\t\t- leftDependentEdges: ";
        for (MyEdge e : this.leftDependentEdges) {
            result += "\n\t\t\t\t- " + e;
        }
        result += "\n\t\t\t- rightDependentEdges: ";
        for (MyEdge e : this.rightDependentEdges) {
            result += "\n\t\t\t\t- " + e;
        }
        return result;
    }

    @Override
    public int compareTo(MyDependencyNode o) {
        return this.id - o.getId();
    }
    
    public int getBeginPos(){
        return this.beginPos;
    }
    
    public int getEndPos(){
        return this.endPos;
    }
}