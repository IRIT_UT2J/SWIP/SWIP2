/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.irit.swip2.nltopivot.parser;

import edu.stanford.nlp.util.Pair;
import fr.irit.swip2.SWIPWorkflow;
import fr.irit.swip2.nltopivot.parser.model.DependencyTree;
import fr.irit.swip2.utils.OpenQASwipParameters;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.aksw.openqa.Properties;
import org.aksw.openqa.component.answerformulation.AbstractQueryParser;
import org.aksw.openqa.component.context.IContext;
import org.aksw.openqa.component.param.IParamMap;
import org.aksw.openqa.component.param.IResultMap;
import org.aksw.openqa.component.providers.impl.ServiceProvider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Fabien
 */
public abstract class AbstractDependenciesParser extends AbstractQueryParser{

	private static final Logger LOGGER = LogManager.getLogger(AbstractDependenciesParser.class);
	
    public AbstractDependenciesParser(Map<String, Object> params) {
        super(params);
    }
    
     @Override
    public boolean canProcess(IParamMap param) {
        return param.contains(Properties.Literal.TEXT);
    }
    
    @Override
    public String getVersion() {
            return "SWIP2-DEP-PARSER"; // version
    }

    @Override
    public abstract String getLabel();

    @Override
    public String getId() {
            return getLabel() + " " + getVersion(); // id = label + version
    }
    
    public abstract HashMap<Pair<Integer, Integer>, String> getNerMap();

    public abstract DependencyTree getDependenciesGraph(String nerQuery);
    
    @Override
    public List<? extends IResultMap> process(IParamMap param, ServiceProvider sp, IContext ic) {
    	LOGGER.info("Parsing for getting dependencies ("+this.getLabel()+")...");
        
        DependencyTree dt = this.getDependenciesGraph(param.getParam(OpenQASwipParameters.NER_QUERY, String.class));
        
        
        LOGGER.debug("Parser "+this.getLabel()+" performed this dependencies tree :");
        LOGGER.debug(dt);
        LOGGER.debug("Head Node : ");
        LOGGER.debug(dt.getHeadNode());
        param.setParam(OpenQASwipParameters.DEP_GRAPH, dt);
        //param.setParam(OpenQASwipParameters.NER_MAP, this.getNerMap());
        param.setParam(OpenQASwipParameters.HEAD_NODE, dt.getHeadNode());
        
        return null;
        
    }
    
}
