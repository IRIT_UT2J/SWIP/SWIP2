/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.irit.swip2.nltopivot.NER.impl;

import fr.irit.swip2.SWIPWorkflow;
import fr.irit.swip2.nltopivot.NER.AbstractNERParser;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Fabien
 */
public class DBpediaSpotlightNERParser extends AbstractNERParser{

	private static final Logger LOGGER = LogManager.getLogger(DBpediaSpotlightNERParser.class);
	
    public DBpediaSpotlightNERParser(){
        super(null);
    }

    @Override
    public String getLabel() {
        return "DBPediaSpotlight";
    }

    @Override
    public HashMap<String, String> getNERFromQuery(String query) {
        String url = "http://spotlight.sztaki.hu:2222/rest/annotate?text="+URLEncoder.encode(query)+"&confidence=0.5&support=0&spotter=Default&disambiguator=Default&policy=whitelist&types=&sparql=";
        HashMap<String, String> ner_corres = new HashMap<>();
        try {
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            
            con.setRequestMethod("GET");
            
            
            int responseCode = con.getResponseCode();
            LOGGER.debug("\nSending 'GET' request to URL : " + url);
            LOGGER.debug("Response Code : " + responseCode);
            
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            
            LOGGER.debug("DBPediaSpotlight Response : "+response.toString());
            
            Pattern p = Pattern.compile("<div>(.*)</div>");
            String texte =   response.toString();
            
            Matcher m = p.matcher(texte);
            String ner_query = query;
            if(m.find()){
                ner_query = m.group(1);
            }
            
            p = Pattern.compile("<a href=\"([^\"]+)\"[^>]+>([^<]+)</a>");
            m = p.matcher(ner_query);
            while(m.find()){
                String uriNER = m.group(1);
                String valNER = m.group(2);
                String underscoredValNER = valNER.replaceAll(" ", "_");
                ner_corres.put(underscoredValNER, uriNER);
            } 
        } catch (Exception ex) {
        	LOGGER.error("DBpediaSpotlight ERROR (connection timeout)");
            
        }
        
        return ner_corres;
    }
    
}
