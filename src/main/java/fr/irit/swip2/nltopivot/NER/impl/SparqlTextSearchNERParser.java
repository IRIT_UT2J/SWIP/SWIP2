/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.irit.swip2.nltopivot.NER.impl;

import java.util.ArrayList;

import fr.irit.swip2.SWIPWorkflow;
import fr.irit.swip2.nltopivot.NER.AbstractNERParser;
import fr.irit.swip2.utils.SourceKB.AbstractSourceKB;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Fabien
 */
public class SparqlTextSearchNERParser extends AbstractNERParser{

    private AbstractSourceKB sourceKB;
    private static final Logger LOGGER = LogManager.getLogger(SparqlTextSearchNERParser.class);
    
    public SparqlTextSearchNERParser(AbstractSourceKB source){
        super(null);
        this.sourceKB = source;
    }
    
    @Override
    public String getLabel() {
        return "SparqlTextSearch";
    }
    
    
     private HashMap<String, String> getNERFromKeywords(String keywords){
        HashMap<String, String> ret = new HashMap<>();
        if(!keywords.isEmpty()){
            String query = "PREFIX text: <http://jena.apache.org/text#>"+
                                    "PREFIX rdfs:    <http://www.w3.org/2000/01/rdf-schema#>"+
                                    "SELECT * {"+
                                    " (?s ?score ?literal) text:query (rdfs:label '"+keywords+"')." +
                                                     
                                    "}" ;
            /*String query =  "PREFIX text: <http://jena.apache.org/text#> "+
                            "PREFIX rdfs:    <http://www.w3.org/2000/01/rdf-schema#> "+
                           "SELECT * WHERE { "+
                           " ?uri rdfs:label ?lit. "+
                            " filter(str(?lit)=\"" + matchedLabel + "\") } ";*/
            

            ResultSet execSelect = this.sourceKB.selectQuery(query);
            while(execSelect.hasNext()){
                QuerySolution sol = execSelect.next();
                LOGGER.debug("SOL : "+sol);
                float score = sol.get("score").asLiteral().getFloat();
                if(score > 2){ //TODO : define affinate threshold
                    ret.put(keywords, sol.get("s").toString());
                }
            }
        }
        
        return ret;
    }
    
     
    public boolean alreadyAnnotated(HashMap<String, String> annotations, String annot){
        boolean ret = false;
        for(String annotKey : annotations.keySet()){
            if(annotKey.contains(annot)){
                ret = true;
                break;
            }
        }
        return ret;
    }
     
    @Override
    public HashMap<String, String> getNERFromQuery(String query){
        HashMap<String, String> ret = new HashMap<>();
        
        
        String q = query.replaceAll("[!?\\.,]", "");
        String[] wordsTemp = q.split("\\s+");
        ArrayList<String> words = new ArrayList<>(Arrays.asList(wordsTemp));
        
        int testSize = words.size()-1;
        while(testSize > 0){
            int first = 0;
            while(first+testSize < words.size()){
                String keywords = "";
                for(int i = first; i <= testSize+first; i++){
                    keywords += words.get(i)+" ";
                }
                keywords = keywords.replaceAll("of", "").trim();
                LOGGER.debug("KEYWORDS : "+keywords);
                HashMap<String, String> results = this.getNERFromKeywords(keywords);
                if(!results.isEmpty()){
                    for(Entry<String, String> e: results.entrySet()){
                        String ner = e.getKey().trim().replace(" ", "_");
                        //ner = ner.substring(0, ner.lastIndexOf("_"));
                        if(!this.alreadyAnnotated(ret, ner)){
                            ret.put(ner, e.getValue());
                            LOGGER.debug("COUCOU :   ----->  "+e.getKey()+" -- "+e.getValue());
                        }
//                        for(String w : e.getKey().split(" ")){
//                           words.remove(w);
//                        }
//                        System.out.println("CURRENT : ");
//                        System.out.println(words);
                    }
                }
                first ++;
            }
            testSize --;
        }
        LOGGER.debug("-------------------");
        LOGGER.debug(ret);
        LOGGER.debug("=====================");
        return ret ; 
    }
    
}
