/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.irit.swip2.nltopivot.NER;

import fr.irit.swip2.SWIPWorkflow;
import fr.irit.swip2.utils.OpenQASwipParameters;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.aksw.openqa.Properties;
import org.aksw.openqa.component.answerformulation.AbstractQueryParser;
import org.aksw.openqa.component.context.IContext;
import org.aksw.openqa.component.param.IParamMap;
import org.aksw.openqa.component.param.IResultMap;
import org.aksw.openqa.component.providers.impl.ServiceProvider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Fabien
 */
public abstract class AbstractNERParser extends AbstractQueryParser{
	
	private static final Logger LOGGER = LogManager.getLogger(AbstractNERParser.class);

    public AbstractNERParser(Map<String, Object> params) {
        super(params);
    }
    
     @Override
    public boolean canProcess(IParamMap param) {
        return param.contains(Properties.Literal.TEXT);
    }
    
    @Override
    public String getVersion() {
            return "SWIP2-NER"; // version
    }

    @Override
    public abstract String getLabel();

    @Override
    public String getId() {
            return getLabel() + " " + getVersion(); // id = label + version
    }

    @Override
    public List<? extends IResultMap> process(IParamMap param, ServiceProvider sp, IContext ic) {        
        String query = (String) param.getParam(Properties.Literal.TEXT);
        LOGGER.debug("Finding NER ("+this.getLabel()+") ...");
        
        HashMap<String, String> ner_corres = this.getNERFromQuery(query);
        param.setParam(OpenQASwipParameters.NER_QUERY, query);
        param.setParam(OpenQASwipParameters.NER_MAP, ner_corres);
        LOGGER.debug("RESULTS :");
        LOGGER.debug("NER QUERY : "+query);
        LOGGER.debug("NER MAP : "+ner_corres);
        return null;
    }
    
    
    
    public abstract HashMap<String, String> getNERFromQuery(String query);
}
