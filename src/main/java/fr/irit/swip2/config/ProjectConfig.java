package fr.irit.swip2.config;

import java.util.List;

/**
 * Entity class representing the configuration of a SWIP project. Parsed from json
 */
public class ProjectConfig {
	private String projectName;
	private String projectDesc;
	private Boolean distant;
	private String kbUri;
	private List<String> localKbs;
	private List<String> remoteKbs;
	private List<String> sampleQuestions;
	private ReasonerType reasoner;
	private Boolean indexURI;
	
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getProjectDesc() {
		return projectDesc;
	}
	public void setProjectDesc(String projectDesc) {
		this.projectDesc = projectDesc;
	}
	public Boolean getDistant() {
		return distant;
	}
	public void setDistant(Boolean distant) {
		this.distant = distant;
	}
	public String getKbUri() {
		return kbUri;
	}
	public void setKbUri(String distantUri) {
		this.kbUri = distantUri;
	}
	public ReasonerType getReasoner() {
		return reasoner;
	}
	public void setReasoner(String reasoner) {
		this.reasoner = ReasonerType.getReasonerByType(reasoner);
	}
	public Boolean getIndexURI() {
		return indexURI;
	}
	public void setIndexURI(Boolean indexURI) {
		this.indexURI = indexURI;
	}
	public List<String> getLocalKbs() {
		return localKbs;
	}
	public void setLocalKbs(List<String> localKbs) {
		this.localKbs = localKbs;
	}
	public List<String> getRemoteKbs() {
		return remoteKbs;
	}
	public void setRemoteKbs(List<String> remoteKbs) {
		this.remoteKbs = remoteKbs;
	}
	public List<String> getSampleQuestions() {
		return sampleQuestions;
	}
	public void setSampleQuestions(List<String> sampleQuestions) {
		this.sampleQuestions = sampleQuestions;
	}
    
}
