/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.irit.swip2.config;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Fabien
 */
public class ProjectConfigFileParser {
	private static final Logger LOGGER = LogManager.getLogger(ProjectConfigFileParser.class);
    
    private ProjectConfig config;
    public ProjectConfigFileParser(File projectConfigFile){
        ObjectMapper mapper = new ObjectMapper();
        if(projectConfigFile != null){
            try {
            	
                this.config = mapper.readValue(projectConfigFile, ProjectConfig.class);
            } catch (JsonParseException ex) {
                LOGGER.error("Parsing failed");
                ex.printStackTrace();
            } catch (JsonMappingException e) {
            	LOGGER.error("Mapping exception");
				e.printStackTrace();
			} catch (IOException e) {
				LOGGER.error("IO exception");
				e.printStackTrace();
			}
            
        }
    }
    
    public ProjectConfig getConfig(){
    	return this.config;
    }
    public String getProjectName(){
        return this.config.getProjectName();
    }
    public String getProjectDesc(){
        return this.config.getProjectDesc();
    }
    public Boolean isDistant(){
        return this.config.getDistant();
    }
    public String getKbUri(){
        return this.config.getKbUri();
    }
    public ReasonerType getReasonning(){
        return this.config.getReasoner();
    }
    public Boolean getIndexURI(){
    	return this.config.getIndexURI();
    }
    public List<String> getLocalKbs(){
    	return this.config.getLocalKbs();
    }
    public List<String> getRemoteKbs(){
    	return this.config.getRemoteKbs();
    }
    
}
