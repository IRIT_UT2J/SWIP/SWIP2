package fr.irit.swip2.pivottomappings.model.patterns.mapping;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.irit.swip2.pivottomappings.PivotToMappings;
import fr.irit.swip2.pivottomappings.model.patterns.patternElement.PatternElement;
import fr.irit.swip2.pivottomappings.model.query.queryElement.QueryElement;
import fr.irit.swip2.utils.sparql.SparqlServer;


public class LiteralElementMapping extends ElementMapping {
	
	private static final Logger LOGGER = LogManager.getLogger(PivotToMappings.class);

    public LiteralElementMapping() {
    }

    public LiteralElementMapping(PatternElement pe, QueryElement qe, float trustMark, ElementMapping impliedBy) {
        super(pe, qe, trustMark, impliedBy);
    }

    @Override
    public String getStringForSentence(SparqlServer sparqlServer, String lang) {
        if (this.queryElement.isQueried()) {
            return this.patternElement.getDefaultStringForSentence(sparqlServer);
        }
        String s = this.queryElement.getStringValue();
        LOGGER.debug("String for sentence LEM : "+s);
        return s;
    }
}
