package fr.irit.swip2.pivottomappings.model.query.subquery;

import fr.irit.swip2.pivottomappings.model.query.queryElement.QueryElement;


/**
 * abstract class representing a user subquery
 */
public abstract class Subquery {

    abstract Iterable<QueryElement> getQueryElements();

}
