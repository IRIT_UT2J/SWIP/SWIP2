package fr.irit.swip2.pivottomappings.model.patterns.patternElement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import fr.irit.swip2.pivottomappings.controller.Controller;
import fr.irit.swip2.pivottomappings.exceptions.LiteralException;
import fr.irit.swip2.pivottomappings.exceptions.PatternsParsingException;
import fr.irit.swip2.pivottomappings.model.patterns.mapping.ElementMapping;
import fr.irit.swip2.pivottomappings.model.patterns.mapping.LiteralElementMapping;
import fr.irit.swip2.pivottomappings.model.query.queryElement.QueryElement;
import fr.irit.swip2.utils.sparql.SparqlServer;
import java.util.logging.Level;
import org.json.JSONException;
import org.json.JSONObject;

public class LiteralPatternElement extends PatternElement {
    
    private static final Logger logger = LogManager.getLogger(LiteralPatternElement.class);

    String type = null;
//    List<LiteralElementMapping> possibleMappings = null;

    public LiteralPatternElement(String projectName) {
        super(projectName);
    }
    
    public LiteralPatternElement(JSONObject jsonPatternElement, String projectName) {
        //this.fromJSON(jsonPatternElement);
        super(jsonPatternElement, projectName);
    }

    public LiteralPatternElement(int id, String typeString, String projectName) throws LiteralException {
        super(projectName);
//        this.possibleMappings = new LinkedList<LiteralElementMapping>();
        this.id = id;
        this.type = typeString;
        this.qualifying = true;
    }

//    public List<LiteralElementMapping> getPossibleMappings() {
//        return possibleMappings;
//    }

//    @Override
//    List<? extends ElementMapping> getElementMappings() {
//        return this.getPossibleMappings();
//    }

//    public void setPossibleMappings(List<LiteralElementMapping> possibleMappings) {
//        this.possibleMappings = possibleMappings;
//    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public void preprocess(SparqlServer sparqlServer, String sparqlEndpointURI) throws PatternsParsingException {
//        addPatternElementMatching(Literal.getUriFromType(type), new PatternElementMatching(this, 1));
        addPatternElementMatching(type, new PatternElementMatching(this, 1));
    }

    public void addLiteralMapping(QueryElement qe, float trustMark, ElementMapping impliedBy) {
        Controller.getInstance(this.projectName).addElementMappingForPatternElement(new LiteralElementMapping(this, qe, trustMark, impliedBy), this);
    }

    @Override
    public String toString() {
        return "literal of type " + type.toString() + " - id=" + id;
    }
    
    public String getCanonicalType(){
        return LiteralPatternElement.class.getCanonicalName();
    }
    
     @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject ret = new JSONObject();
        
        ret.put("id", this.id);
        ret.put("qualifying", this.qualifying);
        ret.put("type", this.type);
        ret.put("mappingConditions", this.mappingCondition);
        ret.put("mappingCompulsory", this.mappingIsCompulsory);
        ret.put("maxOccurences", this.maxOccurrences);
        
        return ret;
    }
    
    @Override
    public void fromJSON(JSONObject jsonPatternElement) {
       
        
        try {
            this.id = jsonPatternElement.getInt("id");
            this.qualifying = jsonPatternElement.getBoolean("qualifying");
            this.type = jsonPatternElement.getString("type");
            this.mappingCondition = jsonPatternElement.getString("mappingConditions");
            this.mappingIsCompulsory = jsonPatternElement.getBoolean("mappingCompulsory");
            this.maxOccurrences = jsonPatternElement.getInt("maxOccurences");
        } catch (JSONException ex) {
            java.util.logging.Logger.getLogger(LiteralPatternElement.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public String getDefaultStringForSentence(SparqlServer sparqlServer) {
        if (type.equals("http://www.w3.org/2001/XMLSchema#dateTime")) {
            return "some date";
        } else if (type.equals("year")) {
            return "some year";
        } else {
            return "some pouet";
        }
    }

    @Override
    public void resetForNewQuery() {
//        this.possibleMappings = new LinkedList<LiteralElementMapping>();
        this.mappingIsCompulsory = false;
    }
}
