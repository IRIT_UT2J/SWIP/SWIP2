package fr.irit.swip2.pivottomappings.model.patterns.mapping;

import fr.irit.swip2.pivottomappings.PivotToMappings;
import fr.irit.swip2.pivottomappings.model.patterns.patternElement.PatternElement;
import fr.irit.swip2.pivottomappings.model.query.queryElement.QueryElement;
import fr.irit.swip2.utils.sparql.SparqlServer;
import java.util.ArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public abstract class ElementMapping {
    private static final Logger LOGGER = LogManager.getLogger(PivotToMappings.class);

    PatternElement patternElement = null;
    QueryElement queryElement = null;
    float trustMark;
    ElementMapping impliedBy = null;

    String sparqlConditions = null;
    String varName =null;
    
    private String var;
    private String sparqlTriples;
    private String sentencePart;
    private ArrayList<String> sparqlGen;
    private ArrayList<String> sentenceGen;
    
    
    public ElementMapping() {
        this.sparqlGen = new ArrayList<>();
        this.sentenceGen = new ArrayList<>();
    }

    public ElementMapping(PatternElement patternElement, QueryElement queryElement, float trustMark, ElementMapping impliedBy) {
        this.patternElement = patternElement;
        this.queryElement = queryElement;
        this.trustMark = trustMark;
        this.impliedBy = impliedBy;
    }

    public ElementMapping getImpliedBy() {
        return impliedBy;
    }

    public void setImpliedBy(ElementMapping impliedBy) {
        this.impliedBy = impliedBy;
    }

    public PatternElement getPatternElement() {
        return patternElement;
    }

    public void setPatternElement(PatternElement patternElement) {
        this.patternElement = patternElement;
    }

    public QueryElement getQueryElement() {
        return queryElement;
    }

    public void setQueryElement(QueryElement queryElement) {
        this.queryElement = queryElement;
    }

    public float getTrustMark() {
        return trustMark;
    }

    public void setTrustMark(float trustMark) {
        this.trustMark = trustMark;
    }
    
    public void setSparqlConditions(String sparqlConds){
        if(this.sparqlConditions == null){
            this.sparqlConditions = "";
        }
        if(!this.sparqlConditions.contains(sparqlConds))
            this.sparqlConditions +=sparqlConds;
        LOGGER.debug("SPARQL CONDITIONS : "+this.sparqlConditions);
    }
        
    abstract public String getStringForSentence(SparqlServer sparqlServer, String lang);

    @Override
    public String toString() {
        return patternElement + " -> " + queryElement + " - trust mark=" + trustMark;
    }
}
