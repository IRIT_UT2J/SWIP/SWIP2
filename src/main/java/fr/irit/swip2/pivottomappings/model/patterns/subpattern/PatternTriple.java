package fr.irit.swip2.pivottomappings.model.patterns.subpattern;

import fr.irit.swip2.pivottomappings.PivotToMappings;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.Logger;
import fr.irit.swip2.pivottomappings.controller.Controller;
import fr.irit.swip2.pivottomappings.model.KbTypeEnum;
import fr.irit.swip2.pivottomappings.model.patterns.Pattern;
import fr.irit.swip2.pivottomappings.model.patterns.mapping.ElementMapping;
import fr.irit.swip2.pivottomappings.model.patterns.mapping.InstanceAndClassElementMapping;
import fr.irit.swip2.pivottomappings.model.patterns.mapping.KbElementMapping;
import fr.irit.swip2.pivottomappings.model.patterns.mapping.PatternToQueryMapping;
import fr.irit.swip2.pivottomappings.model.patterns.patternElement.ClassPatternElement;
import fr.irit.swip2.pivottomappings.model.patterns.patternElement.KbPatternElement;
import fr.irit.swip2.pivottomappings.model.patterns.patternElement.LiteralPatternElement;
import fr.irit.swip2.pivottomappings.model.patterns.patternElement.PatternElement;
import fr.irit.swip2.pivottomappings.model.patterns.patternElement.PropertyPatternElement;
import fr.irit.swip2.pivottomappings.model.query.queryElement.Keyword;
import fr.irit.swip2.pivottomappings.model.query.queryElement.Literal;
import fr.irit.swip2.utils.sparql.SparqlServer;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.logging.Level;
import org.apache.logging.log4j.LogManager;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * class representing a subpattern (triple e1, e2, e3)
 */
public class PatternTriple extends Subpattern {

    private static final Logger LOGGER = LogManager.getLogger(PivotToMappings.class);
    private ClassPatternElement e1 = null;
    private PropertyPatternElement e2 = null;
    private PatternElement e3 = null;

    public PatternTriple(String projectName) {
        super(projectName);
    }
    
    public PatternTriple(JSONObject patternTripleJSON, String projectName){
        super(patternTripleJSON, projectName);
    }

    public PatternTriple(ClassPatternElement pe1, PropertyPatternElement pe2, PatternElement pe3, String projectName) {
        super(projectName);
        e1 = pe1;
        e2 = pe2;
        e3 = pe3;
    }

    public ClassPatternElement getE1() {
        return e1;
    }

    public void setE1(ClassPatternElement e1) {
        this.e1 = e1;
    }

    public PropertyPatternElement getE2() {
        return e2;
    }

    public void setE2(PropertyPatternElement e2) {
        this.e2 = e2;
    }

    public PatternElement getE3() {
        return e3;
    }

    public void setE3(PatternElement e3) {
        this.e3 = e3;
    }

    @Override
    public String toString() {
        return " + - " + e1.toString() + "\n   - " + e2.toString() + "\n   - " + e3.toString();
    }
    
    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject ret = super.toJSON();
        try {
            ret.put("e1", this.e1.toJSON());
            ret.put("e2", this.e2.toJSON());
            ret.put("e3", this.e3.toJSON());
        } catch (JSONException ex) {
            System.out.println("ERROR DURING EXPORTING PATTERN : "+ex);
        }
        return ret;
    }
    
    @Override
    public void fromJSON(JSONObject jsonSubPattern, String projectName) {
        this.projectName = projectName;
        try {
            this.e1 = new ClassPatternElement(jsonSubPattern.getJSONObject("e1"), projectName);
            this.e2 = new PropertyPatternElement(jsonSubPattern.getJSONObject("e2"), projectName);
            JSONObject e3JSON = jsonSubPattern.getJSONObject("e3");
            if(e3JSON.has("canonicaleType")){
                try {
                    //this.e3 = canonicaleTypeClass.newInstance();
                    Class patternElementClass = Class.forName(e3JSON.getString("canonicaleType"));
                    Constructor con = patternElementClass.getConstructor(JSONObject.class, String.class);
                    this.e3 = (PatternElement) con.newInstance(e3JSON, this.projectName);
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | SecurityException | IllegalArgumentException | InvocationTargetException ex) {
                    java.util.logging.Logger.getLogger(PatternTriple.class.getName()).log(Level.SEVERE, null, ex);
                    System.err.println("ERROR PATTERN TRIPLE : ");
                    System.err.println(ex);
                }
            }
            
        } catch (JSONException ex) {
            java.util.logging.Logger.getLogger(PatternTriple.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public HashMap<Integer, PatternElement> getPatternElements(){
        HashMap<Integer, PatternElement> ret = new HashMap<>();
        
        ret.put(this.e1.getId(), this.e1);
        ret.put(this.e2.getId(), this.e2);
        ret.put(this.e3.getId(), this.e3);
        
        return ret;
    }

    public String getCanonicaleType(){
        return PatternTriple.class.getCanonicalName();
    }
    
    @Override
    public String toStringWithMapping(PatternToQueryMapping ptqm) {
        return "   + " + e1.toStringWithMapping(ptqm)
                + "\n     " + e2.toStringWithMapping(ptqm)
                + "\n     " + e3.toStringWithMapping(ptqm);
    }

    @Override
    public String generateSparqlWhere(PatternToQueryMapping ptqm, SparqlServer sparqlServer, Map<PatternElement, String> elementsStrings, Set<String> selectElements, HashMap<String, String> numerciDataPropertyElements, LinkedList<String> typeStrings, LinkedList<String> labelStrings) {
//        LinkedList<String> typeStrings = new LinkedList<String>();
//        LinkedList<String> labelStrings = new LinkedList<String>();
        HashMap<PatternElement, String> matchNumerciDataProperty = new HashMap<PatternElement, String>();
        String sparqlE1 = sparqlForElement(e1, typeStrings, labelStrings, ptqm, sparqlServer, elementsStrings, selectElements, matchNumerciDataProperty);
        String sparqlE2 = sparqlForElement(e2, typeStrings, labelStrings, ptqm, sparqlServer, elementsStrings, selectElements, matchNumerciDataProperty);
        String sparqlE3 = sparqlForElement(e3, typeStrings, labelStrings, ptqm, sparqlServer, elementsStrings, selectElements, matchNumerciDataProperty);
        String result = "       "
                + sparqlE1 + " "
                + sparqlE2 + " "
                + sparqlE3 + ".\n";

        if (!matchNumerciDataProperty.isEmpty()) {
            numerciDataPropertyElements.put(matchNumerciDataProperty.get(e2), sparqlE3);
        }

//        for (String typeString : typeStrings) {
//            result = typeString + result;
//        }
//
//        for (String labelString : labelStrings) {
//            result = labelString + result;
//        }

        return result;
    }

    private String sparqlForElement(
            PatternElement e, 
            LinkedList<String> typeStrings, 
            LinkedList<String> labelStrings, 
            PatternToQueryMapping ptqm, 
            SparqlServer sparqlServer, 
            Map<PatternElement, String> elementsStrings, 
            Set<String> selectElements, 
            HashMap<PatternElement, String> numerciDataPropertyElements) {
        
        String elementString = elementsStrings.get(e);
        if (elementString == null) {
            List<ElementMapping> elementMappings = ptqm.getElementMappings(e);
            if (!elementMappings.isEmpty()) { // element mapped
                ElementMapping elementMapping = elementMappings.get(0);
                
                elementString = innerSparqlForElement( e, typeStrings,  labelStrings,  ptqm,  sparqlServer, 
                        selectElements,  numerciDataPropertyElements, elementMapping);
                
            } else { // element not mapped
                if (e instanceof ClassPatternElement) {
                    elementString = "?var" + ++(Subpattern.varCount);
                    String type = ((ClassPatternElement) e).getUri();
                    if (!type.equals("BlankNode")) {
                        typeStrings.add("       " + elementString + " rdf:type <" + type + ">.");
                    }
                } else if (e instanceof PropertyPatternElement) {
                    elementString = "<" + ((PropertyPatternElement) e).getUri() + ">";
                } else { // literal
                    elementString = "?var" + ++(Subpattern.varCount);
                    // TODO: eventuellemnt contraindre le type du literal avec FILTER (datatype...
                }
            }
            elementsStrings.put(e, elementString);
        }
        return elementString;
    }

    private String innerSparqlForElement(
            PatternElement e,
            LinkedList<String> typeStrings,
            LinkedList<String> labelStrings,
            PatternToQueryMapping ptqm,
            SparqlServer sparqlServer,
            Set<String> selectElements,
            HashMap<PatternElement, String> numerciDataPropertyElements,
            ElementMapping elementMapping) {

        String elementString = "";
        String varName = elementMapping.getQueryElement().getVarName()+"_"+e.getId();
        //String generalizedElem = "";
        if (elementMapping instanceof KbElementMapping) {
            KbElementMapping kbElementMapping = (KbElementMapping) elementMapping;
            String firstlyMatchedOntResource = kbElementMapping.getFirstlyMatchedOntResourceUri();

            String toInsert = "";
            if (kbElementMapping.isGeneralized()) {
                toInsert = "{{gen" + kbElementMapping.getPatternElement().getId() + "}}";
            } else {
                toInsert = "<" + kbElementMapping.getFirstlyMatchedOntResourceUri() + ">";
            }

            if (sparqlServer.isClass(firstlyMatchedOntResource)) { // class
                //elementString = "?var" + ++(Subpattern.varCount);
                elementString = varName;
                String typeDeclaration = "       " + elementString + " rdf:type " + toInsert + ".\n";
                if (!typeStrings.contains(typeDeclaration)) {
                    typeStrings.add(typeDeclaration);
                }
                if (kbElementMapping.getQueryElement().isQueried()) {
                    selectElements.add(elementString);
                }
            } else if (Keyword.pseudoClasses.containsKey(firstlyMatchedOntResource)) { // pseudoclas
                elementString = varName;
                String typeDeclaration = "       " + elementString + " " + Keyword.jokerTypePropertiesString + " " + toInsert + ".\n";
                if (!typeStrings.contains(typeDeclaration)) {
                    typeStrings.add(typeDeclaration);
                }
                if (kbElementMapping.getQueryElement().isQueried()) {
                    selectElements.add(elementString);
                }
            } else if (kbElementMapping.getKbType() == KbTypeEnum.NUMDATAPROP) {
                numerciDataPropertyElements.put(e, kbElementMapping.getQueryElement().getStringValue());
                elementString = toInsert;
            } else if (sparqlServer.isProperty(firstlyMatchedOntResource)) { // property
                elementString = toInsert;
            } else { // instance
                //elementString = "?var" + ++(Subpattern.varCount);
                elementString = varName;
                List<String> types = sparqlServer.listTypes(firstlyMatchedOntResource);
                String sparqlConds = "";
//                ArrayList<String> testTypesSparqlConds = new ArrayList<>();
//                for (String type : types) {
////                    if(! sparqlServer.isClass(toInsert)){
////                        List<String> classTypes = sparqlServer.listTypes(toInsert);
////                        toInsert = classTypes.get(0);
////                    }
//                    if(!type.startsWith("<")){
//                        type = "<"+type+">";
//                    }
//                    String typeDeclaration = "       " + elementString + " rdf:type " + type + ".\n";
//                    if (!testTypesSparqlConds.contains(typeDeclaration)) {
//                        testTypesSparqlConds.add(typeDeclaration);
//                        sparqlConds += typeDeclaration;
//                    }
//                }
                //PatternElement pe = pivotKbElementMapping.getPatternElement();
                String t = toInsert;
                if(!types.isEmpty()){
                    t = "<"+types.get(0)+">";
                }
                String typeDeclaration = "       " + elementString + " rdf:type " + t + ".\n";
                sparqlConds += typeDeclaration;
                String matchedLabel = ((KbElementMapping) ptqm.getElementMappings(e).get(0)).getBestLabel();
                String labelVar = "?label"+kbElementMapping.getPatternElement().getId();
                if(matchedLabel.contains("@"))
                    matchedLabel = matchedLabel.substring(0, matchedLabel.lastIndexOf("@"));
                String labelS ="       " + elementString + " (rdfs:label|dc:title|foaf:name) "+labelVar+". \n"
                        + "filter(str("+labelVar+")=\"" + matchedLabel + "\")\n";
                //labelStrings.add(labelS);
                //elementString = "{{gen" + kbElementMapping.getPatternElement().getId() + "}}";
                sparqlConds += labelS;
                kbElementMapping.setSparqlConditions(sparqlConds);
                typeStrings.add("{{gen" + kbElementMapping.getPatternElement().getId() + "}}");
            }
        } else if (elementMapping instanceof InstanceAndClassElementMapping) {
            elementString = varName;
            String sparqlConds = "";
            sparqlConds += "       " + elementString + " " + Keyword.jokerTypePropertiesStringWithType + " <" + ((InstanceAndClassElementMapping) elementMapping).getFirstlyMatchedClass() + ">.\n";
            sparqlConds += "       " + elementString + " (rdfs:label|dc:title|foaf:name) \"" + ((InstanceAndClassElementMapping) elementMapping).getBestLabelInstance() + "\".\n";
            typeStrings.add("{{gen" + elementMapping.getPatternElement().getId() + "}}");
            elementMapping.setSparqlConditions(sparqlConds);
        } else { // literal
            System.out.println("Lit : "+varName);
            if (elementMapping.getQueryElement().isQueried()) {
//                        elementString = "?literal" + ++(Subpattern.varCount);
                elementString = varName;
                // TODO: eventuellemnt contraindre le type du literal avec FILTER (datatype...
                selectElements.add(elementString);
            } else {
                elementString = ((Literal) elementMapping.getQueryElement()).getStringForSparql(labelStrings);
            }
        }
        return elementString;

    }

    public boolean contains(PatternElement pe) {
        return (this.e1 == pe || this.e2 == pe || this.e3 == pe);
    }

    @Override
    public void finalizeMapping(SparqlServer serv, Pattern p) {
        // handle pattern elements refered by property pattern elements
        List<PatternElement> elementsToMap = new LinkedList<PatternElement>();
        for (int id : this.e2.getReferedElements()) {
            elementsToMap.add(p.getPatternElementById(id));
        }
        for (ElementMapping em2 : Controller.getInstance(this.projectName).getElementMappingsForPatternElement(this.e2)) {
            if (!em2.getQueryElement().getRoles().usedAsProperty()) {
                for (PatternElement elementToMap : elementsToMap) {
                    if (elementToMap instanceof KbPatternElement) {
                        KbPatternElement kbe = (KbPatternElement) elementToMap;
                        kbe.addKbMapping(em2.getQueryElement(), em2.getTrustMark(), kbe.getUri(), serv.getALabel(kbe.getUri()), em2, KbTypeEnum.CLASS);
                    } else if (elementToMap instanceof LiteralPatternElement) {
                        LiteralPatternElement le = (LiteralPatternElement) elementToMap;
                        le.addLiteralMapping(em2.getQueryElement(), em2.getTrustMark(), em2);
                    }
                }
            }
        }
    }
}