package fr.irit.swip2.pivottomappings.model.patterns.patternElement;

import java.util.LinkedList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import fr.irit.swip2.pivottomappings.controller.Controller;
import fr.irit.swip2.pivottomappings.model.KbTypeEnum;
import fr.irit.swip2.pivottomappings.model.patterns.mapping.ElementMapping;
import fr.irit.swip2.pivottomappings.model.patterns.mapping.InstanceAndClassElementMapping;
import fr.irit.swip2.pivottomappings.model.patterns.mapping.KbElementMapping;
import fr.irit.swip2.pivottomappings.model.query.Query;
import fr.irit.swip2.pivottomappings.model.query.queryElement.QueryElement;
import fr.irit.swip2.pivottomappings.model.query.subquery.Q2;
import fr.irit.swip2.pivottomappings.model.query.subquery.Subquery;
import fr.irit.swip2.utils.sparql.SparqlServer;
import org.json.JSONObject;


public final class ClassPatternElement extends KbPatternElement {

    private static Logger logger = LogManager.getLogger(ClassPatternElement.class);
    
   
    public ClassPatternElement(String projectName){
        super(projectName);
    }
    
    
    
    public ClassPatternElement(JSONObject jsonPatternElement, String projectName) {
        super(jsonPatternElement, projectName);
    }
    
    public ClassPatternElement(int id, String uri, boolean qualifying, String projectName) {
        super(uri, qualifying, projectName);
        this.id = id;
    }
    
    public String toString(){
        String ret = super.toString();
        
        return ret;
    }
    
    public String getCanonicalType(){
        return ClassPatternElement.class.getCanonicalName();
    }


    @Override
    public String getDefaultStringForSentence(SparqlServer sparqlServer) {
        String label = sparqlServer.getALabel(this.getUri());
        return (label == null ? "no label found" : "un(e) " + label);
    }

    public void addInstanceAndClassMapping(QueryElement qeInst, QueryElement qeClass, float trustMark, String firstlyMatchedInstance, String firstlyMatchedClass, String bestLabelInstance, String bestLabelClass, ElementMapping impliedBy) {
        KbElementMapping test = new KbElementMapping(this, qeInst, trustMark, firstlyMatchedInstance, bestLabelInstance, impliedBy, KbTypeEnum.IND);
        Controller.getInstance(this.projectName).addElementMappingForPatternElement(test, this);
        //Controller.getInstance().addElementMappingForPatternElement(new InstanceAndClassElementMapping(this, qeInst, qeClass, trustMark, firstlyMatchedInstance, firstlyMatchedClass, bestLabelInstance, bestLabelClass, impliedBy), this);
    }

    public void checkForInstanceAndClass(Query userQuery, SparqlServer serv) {
        logger.info("checkForInstanceAndClass: " + this);
        // in two steps to prevent java.util.ConcurrentModificationException
        // step1: find out wich pairs (instance, class) must be added
        List<ElementMapping> emInstToAdd = new LinkedList<ElementMapping>();
        List<ElementMapping> emClassToAdd = new LinkedList<ElementMapping>();
        for (ElementMapping emInst : Controller.getInstance(this.projectName).getElementMappingsForPatternElement(this) ) {
            String firstlyMatchedInstance = ((KbElementMapping) emInst).getFirstlyMatchedOntResourceUri();
            for (ElementMapping emClass : Controller.getInstance(this.projectName).getElementMappingsForPatternElement(this)) {
                String firstlyMatchedClass = ((KbElementMapping) emClass).getFirstlyMatchedOntResourceUri();
                if (emInst != emClass && serv.isInstanceOfClass(firstlyMatchedInstance, firstlyMatchedClass)) {
                    String bestLabelInstance = ((KbElementMapping) emInst).getBestLabel();
                    String bestLabelClass = ((KbElementMapping) emClass).getBestLabel();
                    emInstToAdd.add(emInst);
                    emClassToAdd.add(emClass);
                }
            }
        }
        // step2: add them all to pattern element mappings list
        for (int i = 0; i < emInstToAdd.size(); i++) {
            KbElementMapping emInst = ((KbElementMapping) emInstToAdd.get(i));
            KbElementMapping emClass = ((KbElementMapping) emClassToAdd.get(i));
            String firstlyMatchedInstance = emInst.getFirstlyMatchedOntResourceUri();
            String firstlyMatchedClass = emClass.getFirstlyMatchedOntResourceUri();
            String bestLabelInstance = emInst.getBestLabel();
            String bestLabelClass = emClass.getBestLabel();
            // process trust mark
            QueryElement qeInst = emInst.getQueryElement();
            QueryElement qeClass = emClass.getQueryElement();
            float trustMark = (emInst.getTrustMark() + emClass.getTrustMark()) / (float)2.;
            if (qeInst == qeClass) {
                trustMark /= (float)2.;
            } else {
                for (Subquery sq : userQuery.getSubqueries()) {
                    if (sq instanceof Q2) {
                        QueryElement e1 = ((Q2)sq).getE1();
                        QueryElement e2 = ((Q2)sq).getE2();
                        if ( (e1==qeInst && e2 == qeClass) || (e2==qeInst && e1 == qeClass) ) {
                            trustMark *= (float)2.;
                        }
                    }
                }
            }
            this.addInstanceAndClassMapping(qeInst, qeClass, trustMark, firstlyMatchedInstance, firstlyMatchedClass, bestLabelInstance, bestLabelClass, null);
        }
    }
}
