package fr.irit.swip2.pivottomappings.model.patterns.patternElement;

import org.apache.jena.query.QuerySolution;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import fr.irit.swip2.pivottomappings.PivotToMappings;
import fr.irit.swip2.pivottomappings.controller.Controller;
import fr.irit.swip2.pivottomappings.exceptions.PatternsParsingException;
import fr.irit.swip2.pivottomappings.model.KbTypeEnum;
import fr.irit.swip2.pivottomappings.model.patterns.mapping.ElementMapping;
import fr.irit.swip2.pivottomappings.model.patterns.mapping.KbElementMapping;
import fr.irit.swip2.pivottomappings.model.query.queryElement.QueryElement;
import fr.irit.swip2.utils.sparql.SparqlClient;
import fr.irit.swip2.utils.sparql.SparqlServer;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class KbPatternElement extends PatternElement {

    //private static final Logger logger = Logger.getLogger(KbPatternElement.class);
	private static final Logger logger = LogManager.getLogger(KbPatternElement.class);
    private String uri;
    private static float ancestorCoef = (float) 0.5;
    private static float descendantCoef = (float) 0.9;
//    private List<KbElementMapping> mappings = new LinkedList<KbElementMapping>();

    
    public KbPatternElement(String projectName) {
        super(projectName);
    }

    public KbPatternElement(String uri, boolean qualifying, String projectName) {
        super(projectName);
        this.uri = uri;
        this.qualifying = qualifying;
        this.projectName = projectName;
    }
    
    public KbPatternElement(JSONObject jsonPatternElement, String projectName) {
        super(jsonPatternElement, projectName);
        this.fromJSON(jsonPatternElement);
    }

    /**
     * @return the uri
     */
    public String getUri() {
        return uri;
    }

    /**
     * @param uri the uri to set
     */
    public void setUri(String uri) {
        this.uri = uri;
    }

//    /**
//     * @return the ancestorCoef
//     */
//    public static float getAncestorCoef() {
//        return ancestorCoef;
//    }
//
//    /**
//     * @param aAncestorCoef the ancestorCoef to set
//     */
//    public static void setAncestorCoef(float aAncestorCoef) {
//        ancestorCoef = aAncestorCoef;
//    }
//
//    /**
//     * @return the descendantCoef
//     */
//    public static float getDescendantCoef() {
//        return descendantCoef;
//    }
//
//    /**
//     * @param aDescendantCoef the descendantCoef to set
//     */
//    public static void setDescendantCoef(float aDescendantCoef) {
//        descendantCoef = aDescendantCoef;
//    }
    /**
     * @return the mappings
     */
//    public List<KbElementMapping> getMappings() {
//        return mappings;
//    }
//    @Override
//    public List<? extends ElementMapping> getElementMappings() {
//        return getMappings();
//    }
    /**
     * @param mappings the mappings to set
     */
//    public void setMappings(List<KbElementMapping> mappings) {
//        this.mappings = mappings;
//    }
//    /**
//     * @return the logger
//     */
//    public static Logger getLogger() {
//        return logger;
//    }
//
//    /**
//     * @param aLogger the logger to set
//     */
//    public static void setLogger(Logger aLogger) {
//        logger = aLogger;
//    }
    @Override
    public void preprocess(SparqlServer sparqlServer, String sparqlEndpointURI) throws PatternsParsingException {
        if (qualifying) {
            logger.info("KbPatternElement: " + getUri());
            // FIXME: normalement y'a pas besoin d'ajouter la ressource ici, vu qu'après on ajoute tous ses
            // enfants et qu'elle fait partie de ses enfants (RDFS inferences). mais il semblerait que cela 
            // ne fonctionne pas pour toutes les resources (par ex: http://purl.org/ontology/mo/producer)
            logger.info("add: " + getUri());
            addPatternElementMatching(getUri(), new PatternElementMatching(this, 1));
//            addPatternElementMatchingAncestorsSparql(getUri(), 1, sparqlServer);
            addPatternElementMatchingDescendantsSparql(getUri(), 1, sparqlServer, sparqlEndpointURI);
        }
    }

//    void addPatternElementMatchingAncestorsSparql(String uri, float childrenMark, SparqlServer sparqlServer) throws PatternsParsingException {
//        float mark = childrenMark * KbPatternElement.ancestorCoef;
//        Iterable<QuerySolution> results = null;
//        try {
//            String query = "  PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> "
//                    + "SELECT ?parent "
//                    + "WHERE { "
//                    + "       <" + uri + "> rdfs:subClassOf ?parent. "
//                    + "      } ";
//            results = sparqlServer.select(query);
//        } catch (Exception e) {
//            throw new PatternsParsingException("Error occured while querying sparql endpoint:\n" + e.getMessage());
//        }
//        for (QuerySolution sol : results) {
//            String superClassString = sol.getResource("parent").getURI();
//            addPatternElementMatching(superClassString, new PatternElementMatching(this, mark));
//            addPatternElementMatchingAncestorsSparql(superClassString, mark, sparqlServer);
//        }
//    }
    void addPatternElementMatchingDescendantsSparql(String uri, float childrenMark, SparqlServer sparqlServer, String sparqlEndpointURI) throws PatternsParsingException {
        float mark = childrenMark * KbPatternElement.descendantCoef;
        Iterable<Map<String, String>> results = null;
        try {
            String query = "  PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> "
                    + "SELECT ?child "
                    + "WHERE { "
                    + "       ?child (rdfs:subClassOf|rdfs:subPropertyOf) <" + uri + ">. "
                    + "      } ";
            logger.debug("Query the SparqlEndpoint : "+sparqlEndpointURI);
            logger.debug(query);
            SparqlClient sparqlClient = new SparqlClient(sparqlEndpointURI);
            results = sparqlClient.select(query);
        } catch (Exception e) {
            throw new PatternsParsingException("Error occured while querying sparql endpoint:\n" + e.getMessage());
        }
        for (Map<String, String> sol : results) {
            String subClassString = sol.get("child");
            // FIXME: lié au fixme de la fonction appelante. ce test ne devrait normalement pas être là
            if (!subClassString.equals(uri)) {
                logger.info("add: " + subClassString);
                addPatternElementMatching(subClassString, new PatternElementMatching(this, mark));
//                addPatternElementMatchingDescendantsSparql(subClassString, mark, sparqlServer);
            }
        }
    }

    public void addKbMapping(QueryElement qe, float trustMark, String firstlyMatched, String bestLabel, ElementMapping impliedBy, KbTypeEnum kbType) {
        for (ElementMapping em : Controller.getInstance(this.projectName).getElementMappingsForPatternElement(this)) {
            KbElementMapping kbem = (KbElementMapping) em;
            if (kbem.getQueryElement() == qe && kbem.getFirstlyMatchedOntResourceUri().equals(firstlyMatched)) {
                if (trustMark > kbem.getTrustMark()) {
                    kbem.changeValues(trustMark, bestLabel, kbType);
                }
                return;
            }
        }
        Controller.getInstance(this.projectName).addElementMappingForPatternElement(new KbElementMapping(this, qe, trustMark, firstlyMatched, bestLabel, impliedBy, kbType), this);
    }

    @Override
    public void resetForNewQuery() {
//        setMappings(new LinkedList<KbElementMapping>());
        this.mappingIsCompulsory = false;
    }

    @Override
    public String toString() {
        return "[KbPatternElement]" + getUri() + " - id=" + id + " - qualifying=" + qualifying;
    }
    
    @Override
    public JSONObject toJSON() throws JSONException{
        JSONObject ret = new JSONObject();
        
        ret.put("id", this.id);
        ret.put("qualifying", this.qualifying);
        ret.put("uri", this.uri);
        ret.put("mappingConditions", this.mappingCondition);
        ret.put("mappingCompulsory", this.mappingIsCompulsory);
        ret.put("maxOccurences", this.maxOccurrences);
        ret.put("canonicaleType", this.getCanonicalType());
        
        return ret;
    }
    @Override
    public void fromJSON(JSONObject jsonPatternElement) {
        try {
            this.id = jsonPatternElement.getInt("id");
            this.qualifying = jsonPatternElement.getBoolean("qualifying");
            this.uri = jsonPatternElement.getString("uri");
            if(jsonPatternElement.has("mappingConditions")){
                this.mappingCondition = jsonPatternElement.getString("mappingConditions");
            }
            this.mappingIsCompulsory = jsonPatternElement.getBoolean("mappingCompulsory");
            this.maxOccurrences = jsonPatternElement.getInt("maxOccurences");
        } catch (JSONException ex) {
            //java.util.logging.Logger.getLogger(KbPatternElement.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("ERROR during parsing json from KBPatternElement");
            System.err.println(ex);
        }
        
    }
}
