package fr.irit.swip2.pivottomappings.model.patterns.subpattern;

import fr.irit.swip2.pivottomappings.PivotToMappings;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.Logger;
import fr.irit.swip2.pivottomappings.model.KbTypeEnum;
import fr.irit.swip2.pivottomappings.model.patterns.Pattern;
import fr.irit.swip2.pivottomappings.model.patterns.mapping.ElementMapping;
import fr.irit.swip2.pivottomappings.model.patterns.mapping.KbElementMapping;
import fr.irit.swip2.pivottomappings.model.patterns.mapping.LiteralElementMapping;
import fr.irit.swip2.pivottomappings.model.patterns.mapping.PatternToQueryMapping;
import fr.irit.swip2.pivottomappings.model.patterns.patternElement.ClassPatternElement;
import fr.irit.swip2.pivottomappings.model.patterns.patternElement.KbPatternElement;
import fr.irit.swip2.pivottomappings.model.patterns.patternElement.PatternElement;
import fr.irit.swip2.pivottomappings.model.patterns.patternElement.PropertyPatternElement;
import fr.irit.swip2.pivottomappings.model.query.queryElement.Keyword;
import fr.irit.swip2.pivottomappings.model.query.queryElement.Literal;
import fr.irit.swip2.pivottomappings.model.query.queryElement.QueryElement;
import fr.irit.swip2.utils.sparql.SparqlServer;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.regex.Matcher;
import org.apache.logging.log4j.LogManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SubpatternCollection extends Subpattern {

    private static final Logger LOGGER = LogManager.getLogger(SubpatternCollection.class);
    Collection<Subpattern> subpatterns = new LinkedList<Subpattern>();
    String id = null;
    private int minOccurrences = 1;
    private int maxOccurrences = 1;
    private PatternElement pivotElement = null;

    public SubpatternCollection(String projectName) {
        super(projectName);
    }
    
    public SubpatternCollection(JSONObject jsonPatternElement, String projectName){
        super(jsonPatternElement, projectName);
    }

    public SubpatternCollection(Collection<Subpattern> subpatterns, String id, int minOccurrences, int maxOccurrences, PatternElement pivotElement, String projectName) {
        super(projectName);
        this.subpatterns = subpatterns;
        this.id = id;
        this.minOccurrences = minOccurrences;
        this.maxOccurrences = maxOccurrences;
        this.pivotElement = pivotElement;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getMaxOccurrences() {
        return maxOccurrences;
    }

    public void setMaxOccurrences(int maxOccurrences) {
        this.maxOccurrences = maxOccurrences;
    }

    public int getMinOccurrences() {
        return minOccurrences;
    }

    public void setMinOccurrences(int minOccurrences) {
        this.minOccurrences = minOccurrences;
    }

    @Override
    public PatternElement getPivotElement() {
        return pivotElement;
    }

    public void setPivotElement(PatternElement pivotElement) {
        this.pivotElement = pivotElement;
    }

    public Collection<Subpattern> getSubpatterns() {
        return subpatterns;
    }

    public void setSubpatterns(Collection<Subpattern> subpatterns) {
        this.subpatterns = subpatterns;
    }

    @Override
    public String toString() {
        String result = "[\n";
        for (Subpattern sp : this.subpatterns) {
            result += sp.toString() + "\n";
        }
        result += "] " + this.id + ": ";
        if (minOccurrences == maxOccurrences) {
            result += "exactly " + minOccurrences;
        } else {
            if (maxOccurrences == -1) {
                result += " " + minOccurrences + " or more";
            } else {
                result += "between " + minOccurrences + " and " + maxOccurrences;
            }
        }
        if (maxOccurrences > 1 || maxOccurrences == -1) {
            result += " through " + pivotElement.toString();
        }
        return result;
    }
    
     @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject ret = super.toJSON();
        
        ret.put("id", this.id);
        ret.put("maxOccurences", this.maxOccurrences);
        ret.put("minOccurences", this.minOccurrences);
        ret.put("pivotElement", this.pivotElement.toJSON());
        ArrayList<JSONObject> subP = new ArrayList<>();
        for(Subpattern sub : this.subpatterns){
            subP.add(sub.toJSON());
        }
        ret.put("subpatterns", subP);
        return ret;
    }
    
    @Override
    public void fromJSON(JSONObject jsonPatternElement, String projectName) {
        this.projectName = projectName;
        try {
            this.id = jsonPatternElement.getString("id");
            this.maxOccurrences = jsonPatternElement.getInt("maxOccurences");
            this.minOccurrences = jsonPatternElement.getInt("minOccurences");
            JSONArray subP = jsonPatternElement.getJSONArray("subpatterns");
            HashMap<Integer, PatternElement> subPatternsCorres = new HashMap<>();
            Collection<Subpattern> collSubp = new LinkedList<Subpattern>();
            for(int i = 0; i < subP.length(); i++){
                JSONObject subPo = subP.getJSONObject(i);
                Class<?> subPatternClass = Class.forName(subPo.getString("canonicaleType"));
                if(subPatternClass.getCanonicalName().endsWith("SubpatternCollection")){
                    SubpatternCollection coll = new SubpatternCollection(this.projectName);
                    coll.fromJSON(subPo, projectName);
                    collSubp.add(coll);
                    subPatternsCorres.putAll(coll.getPatternElements());
                }
                else if(subPatternClass.getCanonicalName().endsWith("PatternTriple")){
                    PatternTriple tp = new PatternTriple(this.projectName);
                    tp.fromJSON(subPo, projectName);
                    collSubp.add(tp);
                    subPatternsCorres.putAll(tp.getPatternElements());
                }
                else{
                    System.out.println("ERROR CANONICALE TYPE");
                }
            }
            this.subpatterns = collSubp;
            
            JSONObject pivotElementjson = jsonPatternElement.getJSONObject("pivotElement");
            if(subPatternsCorres.containsKey(pivotElementjson.getInt("id"))){
                this.pivotElement = subPatternsCorres.get(pivotElementjson.getInt("id"));
            }
            
        } catch (JSONException | ClassNotFoundException | SecurityException | IllegalArgumentException ex) {
            System.err.println("ERROR PARSING SubPatternCollection");
            System.err.println(ex);
        }
       
    }
    
    public String getCanonicaleType(){
        return SubpatternCollection.class.getCanonicalName();
    }
    
    public HashMap<Integer, PatternElement> getPatternElements(){
        HashMap<Integer, PatternElement> ret = new HashMap<>();
        
        for(Subpattern sp : this.subpatterns){
            ret.putAll(sp.getPatternElements());
        }
        
        return ret;
    }

    public String toStringWithMapping(PatternToQueryMapping ptqm) {
        String result = "";
        for (Subpattern sp : this.subpatterns) {
            result += sp.toStringWithMapping(ptqm) + "\n";
        }
        return result.substring(0, result.length() - 1);
    }

//    public boolean containsAsNonPivot(PatternElement pe) {
//        if (this.pivotElement == pe) {
//            return false;
//        }
//        for (Subpattern sp : this.subpatterns) {
//            if (sp.containsAsNonPivot(pe)) {
//                return true;
//            }
//        }
//        return false;
//    }
    public String modifySentence(String sentence, PatternToQueryMapping ptqm, SparqlServer sparqlServer) {
//        String labelBegin = "-for-" + this.id + "-[";
//        String labelEnd = "]";
//        int beginIndex = sentence.indexOf(labelBegin);
//        int endIndex = sentence.indexOf(labelEnd);
        java.util.regex.Pattern p = java.util.regex.Pattern.compile("-"+this.id+"-\\["
                                                                    + "([^\\]]+)"
                                                                    + "\\]");
        Matcher m = p.matcher(sentence);
        if (m.find()) {
            //String replacementString = sentence.substring(beginIndex + labelBegin.length(), endIndex).replaceAll("\"", "");
            String replacementString = m.group(1);
            int debIndex = m.start();
            int endIndex = m.end();
            String newRepString = "";
            if (this.pivotElement != null) {
                newRepString = generateStringReplacementLoop(replacementString.replaceAll("\"", ""), ptqm, sparqlServer);
            }
            //sentence = "" + sentence.substring(0, beginIndex) + replacementString + sentence.substring(endIndex + labelEnd.length());
            //sentence = sentence.replaceAll("-"+this.id+"-["+replacementString+"]", newRepString);
            sentence = sentence.substring(0, debIndex)+newRepString+sentence.substring(endIndex);
        }
        for (Subpattern sp : this.subpatterns) {
            if (sp instanceof SubpatternCollection) {
                sentence = ((SubpatternCollection) sp).modifySentence(sentence, ptqm, sparqlServer);
            }
        }
        return sentence;
    }

//    private String generateStringReplacementLoop(String s, PatternToQueryMapping ptqm, SparqlServer sparqlServer) {
//        Collection<ElementMapping> ems = ptqm.getElementMappings(this.pivotElement);
//        if (ems.isEmpty()) {
//            // pivot element is not mapped: we don't display the phrase linked to this subpattern collection
//            return "";
//        } else {
//            String result = "";
//            String labelBegin = "_loop_" + this.id + "_(";
//            String labelEnd = ")_end_loop_" + this.id + "_";
//            int beginIndex = s.indexOf(labelBegin);
//            int endIndex = s.indexOf(labelEnd);
//            if (beginIndex == -1 || endIndex == -1) {
//                return s;
//            } else {
//                result += s.substring(0, beginIndex);
//                for (ElementMapping em : ems) {
//                    QueryElement qe = em.getQueryElement();
//                    String queried = "";
//                    if (qe.isQueried()) {
//                        queried = "?";
//                    }
//                    result += s.substring(beginIndex + labelBegin.length(), endIndex).replaceAll("__" + em.getPatternElement().getId() + "__", queried + em.getStringForSentence(sparqlServer, "en") + queried);
//                }
//            }
//            result += s.substring(endIndex + labelEnd.length());
//            return result;
//        }
//    }
    private String generateStringReplacementLoop(String s, PatternToQueryMapping ptqm, SparqlServer sparqlServer) {
        Collection<ElementMapping> ems = ptqm.getElementMappings(this.pivotElement);
        if (ems.isEmpty()) {
            // pivot element is not mapped: we don't display the phrase linked to this subpattern collection
            return "";
        } else {
            for (ElementMapping em : ems) {
                QueryElement qe = em.getQueryElement();
                String queried = "";
                if (qe.isQueried()) {
                    queried = "?";
                }
                String stringForSentence = em.getStringForSentence(sparqlServer, "en");
                if(stringForSentence != null)
                    s = s.replaceAll("-" + em.getPatternElement().getId() + "-", stringForSentence);
                else
                    s = "";
            }
            //result += s.substring(endIndex + labelEnd.length());
            return s;
        }
    }
    
    @Override
    public String generateSparqlWhere(PatternToQueryMapping ptqm, SparqlServer sparqlServer, Map<PatternElement, String> elementsStrings, Set<String> selectElements, HashMap<String, String> numerciDataPropertyElements, LinkedList<String> typeStrings, LinkedList<String> labelStrings) {
        
        
        // if subpattern collection is optional and pivot element is not mapped
        Collection<ElementMapping> pivotElementMappings = ptqm.getElementMappings(this.pivotElement);
        if (this.minOccurrences == 0 && pivotElementMappings.isEmpty()) {
            return "";
        }
        String result = "";
        // TODO: factoriser cette partie avec celle de PatternTriple
        for (ElementMapping pivotElementMapping : pivotElementMappings) {
            String pivotVarName = pivotElementMapping.getQueryElement().getVarName()+"_"+pivotElementMapping.getPatternElement().getId();
            String tempPivotVarName = pivotVarName;
            if (pivotElementMapping instanceof KbElementMapping) {
                KbElementMapping pivotKbElementMapping = (KbElementMapping) pivotElementMapping;
                String firstlyMatchedOntResource = pivotKbElementMapping.getFirstlyMatchedOntResourceUri();

                String toInsert = "";
                if (pivotKbElementMapping.isGeneralized()) {
                    toInsert = "{{gen" + pivotKbElementMapping.getPatternElement().getId() + "}}";
                } else {
                    toInsert = "<" + pivotKbElementMapping.getFirstlyMatchedOntResourceUri() + ">";
                }

                if (sparqlServer.isClass(firstlyMatchedOntResource)) { // class
                    //pivotVarName = "?var" + ++(Subpattern.varCount);
                    LOGGER.debug("GENERALISED CLASS");
//                    result += "       " + pivotVarName + " rdf:type " + toInsert + ".\n";
//                    if (pivotKbElementMapping.getQueryElement().isQueried()) {
//                        selectElements.add(pivotVarName);
//                    }
                    String  elementString = pivotVarName;
                    String typeDeclaration = "       " + elementString + " rdf:type " + toInsert + ".\n";
                    if (!typeStrings.contains(typeDeclaration)) {
                        typeStrings.add(typeDeclaration);
                    }
                    if (pivotKbElementMapping.getQueryElement().isQueried()) {
                        selectElements.add(elementString);
                    }
                } else if (Keyword.pseudoClasses.containsKey(firstlyMatchedOntResource)) { // pseudoclass
                    LOGGER.debug("GENERALISED PSeUDO CLASS");
                    // FIXME: ce cas n'arrive peut-être jamais, à vérifier
                    result += "SI T'AS ÇA DANS LA REQUÊTE, ALORS Y'A UN PROBLÈME!!\n";
                } else if (pivotKbElementMapping.getKbType() == KbTypeEnum.NUMDATAPROP) {
                    LOGGER.debug("GENERALISED NUM DATA PROP");
                    numerciDataPropertyElements.put(pivotKbElementMapping.getBestLabel(), "loutre");
                    pivotVarName = toInsert;
                } else if (sparqlServer.isProperty(firstlyMatchedOntResource)) { // property
                    LOGGER.debug("GENERALISED Property");
                    pivotVarName = toInsert;
                } else { // instance
                    LOGGER.debug("GENERALISED Instance");
                    //pivotVarName = "?var" + ++(Subpattern.varCount);
                    List<String> types = sparqlServer.listTypes(firstlyMatchedOntResource);
//                    for (String type : types) {
                    PatternElement pe = pivotKbElementMapping.getPatternElement();
                    String t = toInsert;
                    if(pe instanceof KbPatternElement){
                        KbPatternElement kpe = (KbPatternElement) pe;
                        t = kpe.getUri();
                    }
                    else if(!types.isEmpty()){
                        t = types.get(0);
                    }
                    result += "       " + pivotVarName + " rdf:type <" + t + ">.\n";
//                    }
                    String matchedLabel = ((KbElementMapping) pivotElementMapping).getBestLabel();
                    /*result += "       { " + pivotVarName + " <http://purl.org/dc/elements/1.1/title> \"" + matchedLabel + "\". } "
                    + "UNION "
                    + "{ " + pivotVarName + " rdfs:label \"" + matchedLabel + "\". }\n";*/
                    //result += "       " + pivotVarName + " (rdfs:label|dc:title|foaf:name) \"" + matchedLabel + "\".\n";
                    String labelVar = "?label"+pivotKbElementMapping.getPatternElement().getId();
                    if(matchedLabel.contains("@"))
                        matchedLabel = matchedLabel.substring(0, matchedLabel.lastIndexOf("@"));
                    String labelS ="       " + pivotVarName + " (rdfs:label|dc:title|foaf:name) "+labelVar+". \n"
                        + "filter(str("+labelVar+")=\"" + matchedLabel + "\")\n";
                    result += labelS;
                    pivotKbElementMapping.setSparqlConditions(result);
                    result = toInsert;
                }
            } else if (pivotElementMapping instanceof LiteralElementMapping) { // literal
                LOGGER.debug("GENERALISED literal");
                if (pivotElementMapping.getQueryElement().isQueried()) {
                    //pivotVarName = "?literal" + ++(Subpattern.varCount);
                    // TODO: eventuellemnt contraindre le type du literal avec FILTER (datatype...
                    selectElements.add(pivotVarName);
                } else {
//                    List<String> typeStrings = new LinkedList<String>();
                    pivotVarName = pivotElementMapping.getQueryElement().getVarName();
                    Literal l = (Literal) (pivotElementMapping.getQueryElement());
                    String filterString = "FILTER ( " + pivotVarName + " = \"" + l.getStringValue() + "\"^^FIXME )";
                    labelStrings.add(filterString);
//                    for (String typeString : typeStrings) {
//                        result += typeString;
//                    }
                }
            }
            elementsStrings.put(this.pivotElement, pivotVarName);
            for (Subpattern sp : this.subpatterns) {
                result += sp.generateSparqlWhere(ptqm, sparqlServer, elementsStrings, selectElements, numerciDataPropertyElements, typeStrings, labelStrings);
            }
        }
        LOGGER.debug("intermediate result : "+result);
        // if pivot element was not mapped but minOccurrences>0
        if (result.equals("")) {
            for (Subpattern sp : this.subpatterns) {
                result += sp.generateSparqlWhere(ptqm, sparqlServer, elementsStrings, selectElements, numerciDataPropertyElements, typeStrings, labelStrings);
            }
        }

        //elementsStrings.remove(this.pivotElement);

        LOGGER.debug("finale result : "+result);
        return result;
    }

    @Override
    public void finalizeMapping(SparqlServer serv, Pattern p) {
        for (Subpattern sp : this.subpatterns) {
            sp.finalizeMapping(serv, p);
        }
    }
}
