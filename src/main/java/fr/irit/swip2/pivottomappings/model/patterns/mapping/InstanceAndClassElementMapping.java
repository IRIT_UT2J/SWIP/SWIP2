package fr.irit.swip2.pivottomappings.model.patterns.mapping;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import fr.irit.swip2.pivottomappings.model.patterns.patternElement.PatternElement;
import fr.irit.swip2.pivottomappings.model.query.queryElement.QueryElement;
import fr.irit.swip2.utils.sparql.SparqlServer;

public class InstanceAndClassElementMapping extends ElementMapping {

    private static final Logger logger = LogManager.getLogger(InstanceAndClassElementMapping.class);
    
    // queryElement field from ElementMapping is the mapped instance
    // mapped class
    QueryElement queryElementClass = null;

    String firstlyMatchedInstance = null;
    String firstlyMatchedClass = null;
    String bestLabelInstance = null;
    String bestLabelClass = null;

    public InstanceAndClassElementMapping() {
    }

    public InstanceAndClassElementMapping(PatternElement pe, QueryElement qeInst, QueryElement qeClass, float trustMark, String firstlyMatchedInstance, String firstlyMatchedClass, String bestLabelInstance, String bestLabelClass, ElementMapping impliedBy) {
        super(pe, qeInst, trustMark, impliedBy);
        this.queryElementClass = qeClass;
        this.firstlyMatchedInstance = firstlyMatchedInstance;
        this.firstlyMatchedClass = firstlyMatchedClass;
        this.bestLabelInstance = bestLabelInstance;
        this.bestLabelClass = bestLabelClass;
    }

    public String getFirstlyMatchedClass() {
        return firstlyMatchedClass;
    }

    public String getBestLabelInstance() {
        return bestLabelInstance;
    }

    @Override
    public String getStringForSentence(SparqlServer sparqlServer, String lang) {
        return bestLabelInstance + "(" + bestLabelClass + ")";
    }

    @Override
    public String toString() {
        return "[InstanceAndClassElementMapping]" + patternElement + " -> " + queryElement + " - trust mark=" + trustMark + " - matched = (" + firstlyMatchedInstance + ", " + firstlyMatchedClass + ") - label = (" + bestLabelInstance + ", " + bestLabelClass + ")";
    }
    
}
