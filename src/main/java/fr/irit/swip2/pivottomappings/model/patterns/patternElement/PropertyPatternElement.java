package fr.irit.swip2.pivottomappings.model.patterns.patternElement;

import java.util.LinkedList;
import java.util.List;
import fr.irit.swip2.utils.sparql.SparqlServer;
import org.json.JSONObject;

public final class PropertyPatternElement extends KbPatternElement {

    private List<Integer> referedElements = new LinkedList<Integer>();

    public PropertyPatternElement(String projectName) {
        super(projectName);
    }
    
    public PropertyPatternElement(JSONObject jsonPatternElement, String projectName) {
        super(jsonPatternElement, projectName);
    }

    public PropertyPatternElement(int id, String uri, boolean qualifying,  List<Integer> referedElements, String projectName) {
        super(uri, qualifying, projectName);
        this.id = id;
        this.referedElements = referedElements;
    }

    /**
     * @return the referedElements
     */
    public List<Integer> getReferedElements() {
        return referedElements;
    }

    /**
     * @param referedElements the referedElements to set
     */
    public void setReferedElements(List<Integer> referedElements) {
        this.referedElements = referedElements;
    }

    @Override
    public String getDefaultStringForSentence(SparqlServer sparqlServer) {
         String label = sparqlServer.getALabel(this.getUri());
        return (label == null ? "no label found" : label);
    }
    
    public String getCanonicalType(){
        return PropertyPatternElement.class.getCanonicalName();
    }

}
