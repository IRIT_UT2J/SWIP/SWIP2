package fr.irit.swip2.pivottomappings.model;


public enum KbTypeEnum {
    CLASS,
    IND,
    PROP,
    DATAPROP,
    NUMDATAPROP,
    NONE
}
