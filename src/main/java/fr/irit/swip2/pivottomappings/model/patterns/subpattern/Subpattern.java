package fr.irit.swip2.pivottomappings.model.patterns.subpattern;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import fr.irit.swip2.pivottomappings.model.patterns.Pattern;
import fr.irit.swip2.pivottomappings.model.patterns.mapping.PatternToQueryMapping;
import fr.irit.swip2.pivottomappings.model.patterns.patternElement.PatternElement;
import fr.irit.swip2.utils.sparql.SparqlServer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

abstract public class Subpattern {

    public static int varCount = 0;
    
    String projectName;

    public Subpattern(String projectName) {
        this.projectName = projectName;
    }
    
    public Subpattern(JSONObject jsonSubPattern, String projectName){
        this.projectName = projectName;
        this.fromJSON(jsonSubPattern, projectName);
    }

    public static int getVarCount() {
        return varCount;
    }

    public static void setVarCount(int varCount) {
        Subpattern.varCount = varCount;
    }
    
    public abstract String toStringWithMapping(PatternToQueryMapping ptqm);

    public abstract String generateSparqlWhere(PatternToQueryMapping ptqm, SparqlServer sparqlServer, Map<PatternElement,String> pivotsNames, Set<String> selectElements, HashMap<String, String> numerciDataPropertyElements, LinkedList<String> typeStrings, LinkedList<String> labelStrings);

    abstract public void finalizeMapping(SparqlServer serv, Pattern p);
    
    public JSONObject toJSON()throws JSONException{
        JSONObject ret = new JSONObject();
        ret.put("canonicaleType", this.getCanonicaleType());
        return ret;
    }
    
    abstract public void fromJSON(JSONObject jsonSubPattern, String projectName);
    
    abstract public String getCanonicaleType();
    
    abstract public HashMap<Integer, PatternElement> getPatternElements();
    
    public PatternElement getPivotElement(){
        return null;
    }
}
