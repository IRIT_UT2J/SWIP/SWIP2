/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.irit.swip2.pivottomappings.model;

import fr.irit.swip2.SWIPWorkflow;
import fr.irit.swip2.pivottomappings.model.patterns.mapping.PatternToQueryMapping;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Fabien
 */
public class MappingsAnswerFormulation {
    private List<PatternToQueryMapping> mappings;
    private String query;
    private String pivotQuery;
    private String projectName;
    
    public MappingsAnswerFormulation(String projectName, String query, String pivotQuery, List<PatternToQueryMapping> mappings){
        this.projectName = projectName;
        this.query = query;
        this.pivotQuery = pivotQuery;
        this.mappings = mappings;
    }
    
    
    public String getProjectName(){
        return this.projectName;
    }
    public String getQuery(){
        return this.query;
    }
    public String getPivotQuery(){
        return this.pivotQuery;
    }
    public List<PatternToQueryMapping> getMappings(){
        return this.mappings;
    }
    
    public String toJSON(){
        JSONObject response = new JSONObject();
        ArrayList<JSONObject> queryResults = new ArrayList<>();
        try{
            if(this.mappings != null){
                for (PatternToQueryMapping ptqm : this.mappings) {
                    JSONObject query = new JSONObject();
                    JSONObject descSentJSon = new JSONObject();
                    JSONObject generalizations = new JSONObject();
                    JSONObject sparqlQuery = new JSONObject();
                    JSONObject uris = new JSONObject();

                    String descSent = ptqm.getSentence().trim();
                    if ( descSent.length() > 0 && descSent.charAt(descSent.length() - 1) == ',') {
                        descSent = descSent.substring(0, descSent.length() - 1);
                    }
                    for (Map.Entry<Integer, ArrayList<String>> entry : ptqm.getGeneralizations().entrySet()) {
                        generalizations.put("gen"+String.valueOf(entry.getKey()), entry.getValue());
                    }
                    descSentJSon.put("string", descSent);
                    descSentJSon.put("generalisation", generalizations);
                    query.put("descriptiveSentence", descSentJSon);

                    for (Map.Entry<Integer, ArrayList<String>> entry : ptqm.getUris().entrySet()) {
                        uris.put(String.valueOf("gen"+entry.getKey()), entry.getValue());
                    }
                    sparqlQuery.put("string", ptqm.getSparqlQuery());
                    sparqlQuery.put("generalisation", uris);
                    query.put("sparqlQuery", sparqlQuery);

                    query.put("mappingDescription", ptqm.getStringDescription());
                    query.put("relevanceMark", String.valueOf(ptqm.getRelevanceMark()));

                    queryResults.add(query);
                    response.put("mappings", queryResults);
                }
            }

            response.put("pivotQuery", pivotQuery);
        }catch (JSONException ex) {
            System.err.println("ERROR DURING JSON GENERATOR aks WS");
        }
        return response.toString();
    }
    
}
