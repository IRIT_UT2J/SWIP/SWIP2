package fr.irit.swip2.pivottomappings.model.patterns;

import fr.irit.swip2.pivottomappings.PivotToMappings;
import java.util.Iterator;
import java.util.List;
import org.apache.logging.log4j.Logger;
import fr.irit.swip2.pivottomappings.controller.Controller;
import fr.irit.swip2.pivottomappings.model.patterns.mapping.PatternToQueryMapping;
import fr.irit.swip2.pivottomappings.model.patterns.patternElement.ClassPatternElement;
import fr.irit.swip2.pivottomappings.model.patterns.patternElement.PatternElement;
import fr.irit.swip2.pivottomappings.model.patterns.subpattern.PatternTriple;
import fr.irit.swip2.pivottomappings.model.patterns.subpattern.Subpattern;
import fr.irit.swip2.pivottomappings.model.patterns.subpattern.SubpatternCollection;
import fr.irit.swip2.pivottomappings.model.query.Query;
import fr.irit.swip2.utils.sparql.SparqlServer;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.logging.log4j.LogManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * class representing a query pattern
 */
public class Pattern {

    private static final Logger LOGGER = LogManager.getLogger(PivotToMappings.class);
    private String name = null;
    private List<Subpattern> subpatterns = null;
    private String projectName;
    /**
     * the set of pattern elements present in the query pattern
     * a same pattern element appears just once, even if it is repeated in the pattern
     */
//    private List<PatternElement> elements = null;
    /**
     * number of possible mappings of the pattern to user queries
     */
    private int numMappingsCombinations = 0;
    /**
     * the template used to generate the sentence presented to the user to explain the result of the mapping
     */
    private String sentenceTemplate = null;

    public Pattern() {
    }

    public Pattern(String name, List<Subpattern> subpatterns, String sentenceTemplate, String projectName) {
        this.name = name;
        this.subpatterns = subpatterns;
        this.sentenceTemplate = sentenceTemplate;
        this.projectName = projectName;
    }
    
    public Pattern(JSONObject jsonPattern, String projectName){
        this.projectName = projectName;
        try {
            this.name =jsonPattern.getString("name");
            this.sentenceTemplate = jsonPattern.getString("sentenceTemplate");
            
            
            ArrayList<Subpattern> subpatterns = new ArrayList<>();
            JSONArray jsonSubPatterns = jsonPattern.getJSONArray("subpatterns");
            for(int i = 0; i < jsonSubPatterns.length(); i++){
                JSONObject subPo = jsonSubPatterns.getJSONObject(i);
                Class subPatternClass = Class.forName(subPo.getString("canonicaleType"));
                Constructor con = subPatternClass.getConstructor(JSONObject.class, String.class);
                Subpattern subpattern = null;
                if(subPatternClass.getCanonicalName().endsWith("SubpatternCollection")){
                    SubpatternCollection coll = new SubpatternCollection(this.projectName);
                    coll.fromJSON(subPo, projectName);
                    subpattern = coll;
                }
                else if(subPatternClass.getCanonicalName().endsWith("PatternTriple")){
                    PatternTriple tp = new PatternTriple(this.projectName);
                    tp.fromJSON(subPo, projectName);
                    subpattern = tp;
                }
                else{
                    System.out.println("ERROR CANONICALE TYPE");
                }
                subpatterns.add(subpattern);
            }
            this.subpatterns = subpatterns;
        } catch (JSONException ex) {
            System.err.println("Error during parsing pattern " +this.name);
            System.err.println(ex);
        } catch (ClassNotFoundException ex) {
            System.err.println("Error during getting class for subpattern");
            System.err.println(ex);
        } catch (NoSuchMethodException | SecurityException | IllegalArgumentException ex) {
            java.util.logging.Logger.getLogger(Pattern.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("Error parsing pattern ");
            System.err.println(ex);
        }
         
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the subpatterns
     */
    public List<Subpattern> getSubpatterns() {
        return subpatterns;
    }

    /**
     * @param subpatterns the subpatterns to set
     */
    public void setSubpatterns(List<Subpattern> subpatterns) {
        this.subpatterns = subpatterns;
    }

//    public List<PatternElement> getElements() {
//        return elements;
//    }
    /**
     * @param elements the elements to set
     */
//    public void setElements(List<PatternElement> elements) {
//        this.elements = elements;
//    }
    public int getNumMappingsCombinations() {
        if (numMappingsCombinations <= 0) {
            generateNumMappingsCombinations();
        }
        return numMappingsCombinations;
    }

    /**
     * @param numMappingsCombinations the numMappingsCombinations to set
     */
    public void setNumMappingsCombinations(int numMappingsCombinations) {
        this.numMappingsCombinations = numMappingsCombinations;
    }

    public String getSentenceTemplate() {
        return sentenceTemplate;
    }

    /**
     * @param sentenceTemplate the sentenceTemplate to set
     */
    public void setSentenceTemplate(String sentenceTemplate) {
        this.sentenceTemplate = sentenceTemplate;
    }

    public void finalizeMappings(Query userQuery, SparqlServer serv) {
        LOGGER.info("START FINALIZE MAPPINGS : "+userQuery+" -- sparql server : "+serv.toString()+" -- "+this.projectName);
        for (Subpattern sp : this.getSubpatterns()) {
            sp.finalizeMapping(serv, this);
        }
        LOGGER.info("FINLIZED MAPPING SUBPATTERN");
        //allow a pattern element to be mapped to a class and an instance of this class in a same elementMapping
        for (PatternElement pe : Controller.getInstance(this.projectName).getPatternElementsForPattern(this)) {
            LOGGER.debug("PATTERN ELEMNT : "+pe);
            if (pe instanceof ClassPatternElement) {
                ((ClassPatternElement)pe).checkForInstanceAndClass(userQuery, serv);
            }
        }
        LOGGER.info("FINALIZE MAPPINGS ENDED");
    }

    private void generateNumMappingsCombinations() {
        int result = 1;
        for (PatternElement pe : Controller.getInstance(this.projectName).getPatternElementsForPattern(this)) {
            result *= pe.calculateNumMappingsCombinations();
        }
        this.numMappingsCombinations = result;
    }

    @Override
    public String toString() {
        String result = "pattern " + this.getName() + ":\n";
        for (Subpattern sp : this.getSubpatterns()) {
            result += sp.toString() + "\n";
        }
        result += "sentence template: " + this.getSentenceTemplate();
        return result;
    }
    
    public JSONObject toJSON() throws JSONException{
        JSONObject ret = new JSONObject();
        
        ret.put("name", this.getName());
        ArrayList<JSONObject> subPatterns = new ArrayList<>();
        for(Subpattern sp : this.getSubpatterns()){
            subPatterns.add(sp.toJSON());
        }
        ret.put("subpatterns", subPatterns);
        ret.put("sentenceTemplate", this.getSentenceTemplate());
        
        return ret;
    }

    public String toStringWithMapping(PatternToQueryMapping ptqm) {
        String result = "";
        for (Subpattern sp : this.getSubpatterns()) {
            result += sp.toStringWithMapping(ptqm) + "\n";
        }
        return result.substring(0, result.length() - 1);
    }

    public void printElementMappings() {
        LOGGER.info(" + pattern " + this.getName() + "(" + this.getNumMappingsCombinations() + " posible query mappings)");
        for (PatternElement pe : Controller.getInstance(this.projectName).getPatternElementsForPattern(this)) {
            pe.printElementMappings();
        }
    }

    public String modifySentence(String sentence, PatternToQueryMapping ptqm, SparqlServer sparqlServer) {
        for (Subpattern sp : this.getSubpatterns()) {
            if (sp instanceof SubpatternCollection) {
                sentence = ((SubpatternCollection) sp).modifySentence(sentence, ptqm, sparqlServer);
            }
        }
        return sentence;
    }

    public ArrayList<PatternToQueryMapping> getBestMappings(int nbMappings, Query userQuery){
        ArrayList<PatternToQueryMapping> result = new ArrayList<>();
        
        
        Iterable<PatternToQueryMapping> it = this.getMappingsIterable();
        for(PatternToQueryMapping ptqm : it){
            ptqm.getRelevanceMark(userQuery);
            result.add(ptqm);
        }
        
        LOGGER.debug("MAPPINGS FOR "+this.name+" : ");
        LOGGER.debug(" -> "+result);
        Collections.sort(result, (PatternToQueryMapping p1, PatternToQueryMapping p2) -> Float.compare(p1.getRelevanceMark(userQuery), p2.getRelevanceMark(userQuery)));
        LOGGER.debug("MAPPINGS SORTED FOR "+this.name+" : ");
        LOGGER.debug(" -> "+result); 
        
        Stream<PatternToQueryMapping> limit = result.stream().limit(nbMappings);
        result = limit.collect(Collectors.toCollection(ArrayList::new));
        
        return result;
    }
    
    public Iterable<PatternToQueryMapping> getMappingsIterable() {
        final String projectName = this.projectName;
        return new Iterable<PatternToQueryMapping>() {

            @Override
            public Iterator<PatternToQueryMapping> iterator() {
                return new Iterator<PatternToQueryMapping>() {

                    List<PatternElement> pes = Controller.getInstance(projectName).getPatternElementsForPattern(Pattern.this);
                    final int numElements = pes.size();
                    long[] numMappings = new long[numElements];
                    int currentMapping = 0;
                    PatternToQueryMapping next;

                    {
                        // instance initializer
                        for (int i = 0; i < numElements; i++) {
                            numMappings[i] = pes.get(i).calculateNumMappingsCombinations();
                        }
                        for (int i = numElements - 2; i >= 0; i--) {
                            numMappings[i] *= numMappings[i + 1];
                        }
                        next = nextForNext();
                    }

                    @Override
                    public boolean hasNext() {
//                        return (currentMapping < numMappings[0]);
                        return (next != null);
                    }

                    @Override
                    public PatternToQueryMapping next() {
                        PatternToQueryMapping buffer = next;
                        next = nextForNext();
                        return buffer;
                    }

                    private PatternToQueryMapping nextForNext() {
                        PatternToQueryMapping ptqm = null;
                        while (ptqm == null && currentMapping < numMappings[0]) {
                            ptqm = new PatternToQueryMapping(Pattern.this, projectName);
                            long localCurrentMapping = currentMapping;
                            for (int i = 0; i < numElements; i++) {
//                                PatternElement pe = Controller.getInstance().getPatternElementsForPattern(Pattern.this).get(i);
                                PatternElement pe = pes.get(i);
                                long numInPe = (i < numElements - 1) ? localCurrentMapping / numMappings[i + 1] : localCurrentMapping;
                                ptqm.addElementMappings(pe.getElementMappings(numInPe));
                                localCurrentMapping = (i < numElements - 1) ? localCurrentMapping % numMappings[i + 1] : 0;
                            }
                            currentMapping++;
                            if (ptqm.isRedundant()) {
                                ptqm = null;
                            }
                        }
                        return ptqm;
                    }

                    @Override
                    public void remove() {
                        throw new UnsupportedOperationException("remove unsupported");
                    }
                };
            }
        };
    }

    public PatternElement getPatternElementById(int id) {
        for (PatternElement pe : Controller.getInstance(this.projectName).getPatternElementsForPattern(this)) {
            if (pe.getId() == id) {
                return pe;
            }
        }
        return null;
    }
}
