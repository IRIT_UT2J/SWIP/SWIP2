package fr.irit.swip2.pivottomappings.model.query.queryElement;

public enum QeRole {

    E1Q1,
    E1Q23,
    E2Q2,
    E2Q3,
    E3Q3
}
