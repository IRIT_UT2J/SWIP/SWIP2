/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.irit.swip2.pivottomappings;

import fr.irit.swip2.SWIPWorkflow;
import fr.irit.swip2.pivottomappings.controller.Controller;
import fr.irit.swip2.pivottomappings.model.patterns.mapping.PatternToQueryMapping;
import fr.irit.swip2.pivottomappings.model.MappingsAnswerFormulation;
import fr.irit.swip2.utils.OpenQASwipParameters;
import fr.irit.swip2.utils.SourceKB.AbstractSourceKB;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.aksw.openqa.Properties;
import org.aksw.openqa.component.answerformulation.AbstractQueryParser;
import org.aksw.openqa.component.context.IContext;
import org.aksw.openqa.component.param.IParamMap;
import org.aksw.openqa.component.param.IResultMap;
import org.aksw.openqa.component.providers.impl.ServiceProvider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Fabien
 */
public class PivotToMappings extends AbstractQueryParser  {
    private static final Logger LOGGER = LogManager.getLogger(PivotToMappings.class);

    private SWIPWorkflow sw;
    private AbstractSourceKB sourceKB;
    
    public PivotToMappings(Map<String, Object> params) {
        super(params);
    }
    public PivotToMappings(SWIPWorkflow sw, AbstractSourceKB source){
        super(null);
        
        this.sw = sw;
        this.sourceKB = source;
    }
    
    @Override
    public boolean canProcess(IParamMap param) {
        return true;
    }
    
    @Override
    public String getVersion() {
            return "SWIP2-PivotToMappings"; // version
    }

    @Override
    public String getLabel(){
        return "P2M";
    }

    @Override
    public String getId() {
            return getLabel() + " " + getVersion(); // id = label + version
    }

    @Override
    public List<? extends IResultMap> process(IParamMap param, ServiceProvider sp, IContext ic) {   
        String pivotQuery = param.getParam(OpenQASwipParameters.PIVOT_QUERY, String.class);
        String query = (String) param.getParam(Properties.Literal.TEXT);
        LOGGER.debug("pivotQuery : "+pivotQuery);
        List<PatternToQueryMapping> bestMappings = Controller.getInstance(this.sw.getProjectName()).getBestMappings(pivotQuery, 50, this.sw.getProjectName());
        LOGGER.debug("best mappings1 : "+bestMappings);
//        if (bestMappings != null) {
//            Collections.reverse(bestMappings);
//        }
        LOGGER.debug("best mappings : "+bestMappings);
        MappingsAnswerFormulation maf = new MappingsAnswerFormulation(this.sw.getProjectName(), query, pivotQuery, bestMappings);
        LOGGER.debug("QUERY : "+query);
        this.sw.addMappingsAnwserFormulation(query, maf);

        
        return null;
    }
    
}
