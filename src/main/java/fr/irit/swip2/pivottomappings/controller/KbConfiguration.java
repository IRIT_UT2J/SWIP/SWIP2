package fr.irit.swip2.pivottomappings.controller;

import fr.irit.swip2.SWIPWorkflow;

public class KbConfiguration
{
    String name;
    String urlKbSparql;
    String patternsPath;
    String lang;
    SWIPWorkflow sw;
    
    
    public KbConfiguration(String name, String urlKbSparql, String patternsPath, String lang, SWIPWorkflow sw)
    {
       this.name = name;
       this.urlKbSparql = urlKbSparql;
       this.patternsPath = patternsPath;
       this.lang = lang;
       this.sw = sw;
    }
    
}
