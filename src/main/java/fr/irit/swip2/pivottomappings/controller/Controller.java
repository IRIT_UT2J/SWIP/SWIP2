package fr.irit.swip2.pivottomappings.controller;

import org.apache.jena.query.QuerySolution;
import fr.irit.swip2.SWIP;
import fr.irit.swip2.SWIPWorkflow;
import fr.irit.swip2.pivottomappings.PivotToMappings;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import org.antlr.runtime.ANTLRInputStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import fr.irit.swip2.pivottomappings.exceptions.KeywordRuntimeException;
import fr.irit.swip2.pivottomappings.exceptions.LexicalErrorRuntimeException;
import fr.irit.swip2.pivottomappings.exceptions.LiteralRuntimeException;
import fr.irit.swip2.pivottomappings.exceptions.PatternsParsingException;
import fr.irit.swip2.pivottomappings.exceptions.QueryParsingException;
import fr.irit.swip2.pivottomappings.exceptions.SyntaxErrorRuntimeException;
import fr.irit.swip2.pivottomappings.model.patterns.Pattern;
import fr.irit.swip2.pivottomappings.model.patterns.antlr.patternsDefinitionGrammarLexer;
import fr.irit.swip2.pivottomappings.model.patterns.antlr.patternsDefinitionGrammarParser;
import fr.irit.swip2.pivottomappings.model.patterns.mapping.ElementMapping;
import fr.irit.swip2.pivottomappings.model.patterns.mapping.PatternToQueryMapping;
import fr.irit.swip2.pivottomappings.model.patterns.patternElement.PatternElement;
import fr.irit.swip2.pivottomappings.model.query.Query;
import fr.irit.swip2.pivottomappings.model.query.antlr.userQueryGrammarLexer;
import fr.irit.swip2.pivottomappings.model.query.antlr.userQueryGrammarParser;
import fr.irit.swip2.utils.sparql.RemoteSparqlServer;
import fr.irit.swip2.utils.sparql.SparqlServer;
import java.io.File;
import java.util.Collections;
import java.util.Map.Entry;
import java.util.logging.Level;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.json.JSONException;

public class Controller {

    private static final Logger LOGGER = LogManager.getLogger(Controller.class);
    private static HashMap<String, Controller> staticController = null;
    //private HashMap<String, KbConfiguration> kbConfs = new HashMap<String, KbConfiguration>();
    private KbConfiguration kbConf;
//    private HashMap<String, SparqlServer> sparqlServers = null;
//    private HashMap<String, SparqlServer> kbSparqlServers = null;
    private SparqlServer kbSparqlServer;
    private HashMap<String, List<Pattern>> patternsMap = null;
    //private HashMap<String, String> langs = null;
    private Map<Pattern, List<PatternElement>> patternElements = new HashMap<Pattern, List<PatternElement>>();
    private Map<PatternElement, List<ElementMapping>> elementMappings = new HashMap<PatternElement, List<ElementMapping>>();
    int nbQuery = 0;
    
    private String projectName;
    
    public Controller(String projectName) {
        this.projectName = projectName;
        this.patternsMap = null;
        
        SWIPWorkflow sw = SWIP.swList.get(this.projectName);
        String kbUri = sw.getKBUri()+"/sparql";
//        if(sw.getKBUri().contains("dbpedia")){
//            kbUri = sw.getKBUri();
//        }
        this.kbConf = new KbConfiguration(this.projectName, kbUri, "projects/"+this.projectName+"/patterns.prdf", "en", sw);
    }

    public static Controller getInstance(String project) {
        if(staticController == null){
            staticController = new HashMap<>();
        }
        if (!staticController.containsKey(project)) {
            
            Controller newContr = new Controller(project);
            staticController.put(project, newContr);
            newContr.createSparqlServer();
            newContr.loadPatterns();
            //newContr.initLangs();
        }
        return staticController.get(project);
    }

    public List<PatternElement> getPatternElementsForPattern(Pattern p) {
        return this.patternElements.get(p);
    }

    public void setPatternElementsForPattern(List<PatternElement> pes, Pattern p) {
        this.patternElements.put(p, pes);
    }

    public List<ElementMapping> getElementMappingsForPatternElement(PatternElement pe) {
        return this.elementMappings.get(pe);
    }

    public void addElementMappingForPatternElement(ElementMapping em, PatternElement pe) {
        this.elementMappings.get(pe).add(em);
    }

//    private void initLangs() {
//        this.langs = new HashMap<String, String>();
//        for (KbConfiguration kbConf : this.kbConfs.values()) {
//            this.langs.put(kbConf.name, kbConf.lang);
//        }
//    }

    private void createSparqlServer() {
        LOGGER.info("Creating sparql server:");
        LOGGER.info("----------------------\n");

//        this.sparqlServers = new HashMap<String, SparqlServer>();
//        this.kbSparqlServers = new HashMap<String, SparqlServer>();

        long time = System.currentTimeMillis();
        //this.sparqlServers.put(kbConf.name, new RemoteSparqlServer(kbConf.urlSparql));
        this.kbSparqlServer =  new RemoteSparqlServer(this.kbConf.urlKbSparql);

        LOGGER.info("Sparql server created");
        LOGGER.info("time for creating sparql server: " + (System.currentTimeMillis() - time) + "ms\n");
        LOGGER.info("================================================================\n");
    }

    
    public void computeLoadedPatterns(List<Pattern> l) throws PatternsParsingException{
        // display loaded patterns
        LOGGER.info("Patterns:\n");
        for (Pattern pattern : l) {
            LOGGER.info(pattern.toString());
        }
        LOGGER.debug("Pattern elements:\n");
        for (List<PatternElement> pes : this.patternElements.values()) {
            for (PatternElement pe : pes) {
                LOGGER.debug(pe.toString());
            }
        }
        LOGGER.info("Pattern loaded");
        LOGGER.info("================================================================\n");
        // preprocess patterns
        LOGGER.info("Preprocessing patterns:");
        LOGGER.info("----------------------\n");
        for (List<PatternElement> pes : this.patternElements.values()) {
            for (PatternElement pe : pes) {
                pe.preprocess(this.kbSparqlServer, this.kbConf.urlKbSparql);
            }
        }
        LOGGER.info("Pattern element matchings:");
        PatternElement.printPatternElementMatchings();
        LOGGER.info("Pattern preprocessed");
        LOGGER.info("================================================================\n");
        this.patternsMap.put(this.kbConf.name, l);
        kbConf.sw.setPatterns(new ArrayList<>(l));
    }
    
    void loadPatterns() {
        if (this.kbSparqlServer == null) {
            LOGGER.info("There is no defined sparql server");
            return;
        } else {
            this.patternsMap = new HashMap<String, List<Pattern>>();

            List<Pattern> l = new LinkedList<Pattern>();
            try {
                LOGGER.info("Loading patterns:");
                LOGGER.info("----------------\n");
                long time = System.currentTimeMillis();
                // read patterns on file and instantiate them
                try {
                    //ANTLRInputStream input = new ANTLRInputStream(this.getClass().getClassLoader().getResourceAsStream(kbConf.patternsPath));
                    String patternsText = FileUtils.readFileToString(new File(this.kbConf.patternsPath));
                    ANTLRInputStream input = new ANTLRInputStream(new ByteArrayInputStream(patternsText.getBytes()));
                    patternsDefinitionGrammarLexer lexer = new patternsDefinitionGrammarLexer(input);
                    CommonTokenStream tokens = new CommonTokenStream(lexer);
                    patternsDefinitionGrammarParser parser = new patternsDefinitionGrammarParser(tokens);
                    l = parser.patterns(this.projectName);
                } catch (RecognitionException ex) {
                    LOGGER.error(ex);
                    throw new PatternsParsingException("RecognitionException: " + ex.getMessage());
                } catch (IOException ex) {
                    LOGGER.error(ex);
                    throw new PatternsParsingException("IOException: " + ex.getMessage());
                } catch (SyntaxErrorRuntimeException ex) {
                    LOGGER.error(ex);
                    throw new PatternsParsingException("Syntax error at " + ex.getMessage());
                } catch (LexicalErrorRuntimeException ex) {
                    LOGGER.error(ex);
                    throw new PatternsParsingException("Syntax error at " + ex.getMessage());
                } catch (RuntimeException ex) {
                    LOGGER.error(ex);
                    throw new PatternsParsingException(ex.getMessage());
                }
                this.computeLoadedPatterns(l);
            } catch (PatternsParsingException ex) {
                LOGGER.info("An error occured while parsing patterns:\n" + ex.getMessage());
                LOGGER.info("Patterns loading aborted");
                LOGGER.error(ex);
            }
        }
    }

    public List<PatternToQueryMapping> getBestMappings(String pivotQueryString, int numMappings, String kbName) {

        // send me an email with the pivot query
        //Mail.sendEmail("[SWIP] getBestMappings query " + nbQuery++, "pivot query: " + pivotQueryString);

        //        PropertyConfigurator.configure(projectPath+"/resources/log4j.properties");
        LOGGER.info("KbName : " + kbName);
        if (this.kbSparqlServer == null) {
            LOGGER.info("There is no defined sparql server");
        } else if (this.patternsMap == null) {
            LOGGER.info("There is no loaded patterns");
        } else {
            try {
                LOGGER.info("Parsing pivot query : " + pivotQueryString);
                final Query userQuery = createQuery(pivotQueryString);
                LOGGER.info("parsed query: " + userQuery.toString() + "\n");
                LOGGER.info("================================================================\n");
                LOGGER.info("Matching query elements to knowledge base and mapping pattern elements:");
                LOGGER.info("----------------------------------------------------------------------\n");
                matchQueryElements(userQuery, kbName);
                LOGGER.info("================================================================\n");
                LOGGER.info("Possible element mappings are:");
                LOGGER.info("-----------------------------\n");
                for (Pattern pattern : this.patternsMap.get(kbName)) {
                    pattern.printElementMappings();
                }
                LOGGER.info("================================================================\n");
                LOGGER.info("Generating and evaluating possible mappings");
                LOGGER.info("--------------------------------------\n");
                // determine the numMappings best mappings
//                PriorityQueue<PatternToQueryMapping> bestMappingsPQTEST = new PriorityQueue<PatternToQueryMapping>(numMappings, new Comparator<PatternToQueryMapping>() {
//
//                    @Override
//                    public int compare(PatternToQueryMapping o1, PatternToQueryMapping o2) {
//                        if (o1.getRelevanceMark(userQuery) < o2.getRelevanceMark(userQuery)) {
//                            return -1;
//                        }
//                        if (o1.getRelevanceMark(userQuery) == o2.getRelevanceMark(userQuery)) {
//                            return 0;
//                        }
//                        return 1;
//                    }
//                });
//                float lowestBestMappingMark = 0;
//                int totalNumMappings = 0;
//                for (Pattern p : this.patternsMap.get(kbName)) {
//                    for (PatternToQueryMapping ptqm : p.getMappingsIterable()) {
//                        totalNumMappings++;
//                        System.out.println("TEST BEST MAPPINGS : "+ptqm.getRelevanceMark(userQuery)+" / "+lowestBestMappingMark);
//                        if (ptqm.getRelevanceMark(userQuery) > lowestBestMappingMark) {
//                            bestMappingsPQTEST.add(ptqm);
//                            System.out.println("NUM MAPPINGS : "+bestMappingsPQTEST.size()+" / "+numMappings);
//                            if (bestMappingsPQTEST.size() > numMappings) {
//                                bestMappingsPQTEST.poll();
//                                lowestBestMappingMark = bestMappingsPQTEST.peek().getRelevanceMark(userQuery);
//                            }
//                        }
//                    }
//                }

                LOGGER.debug("Generating best mappings");
                ArrayList<PatternToQueryMapping> bestMappingsPQ = new ArrayList<>();
                for(Pattern p : this.patternsMap.get(kbName)){
                    ArrayList<PatternToQueryMapping> pMappings = p.getBestMappings(3, userQuery);
                    LOGGER.debug("Adding "+pMappings.size()+" for pattern " +p.getName());
                    bestMappingsPQ.addAll(pMappings);
                }
                LOGGER.debug("Sorting best mappings");
                Collections.sort(bestMappingsPQ, (PatternToQueryMapping p1, PatternToQueryMapping p2) -> Float.compare(p2.getRelevanceMark(), p1.getRelevanceMark()));
                LOGGER.debug("best mappings sorted : "+bestMappingsPQ.size());

                // tranfer best mappings from priority queue to list, store their string description and display them
                final boolean printSentences = true;
                final boolean printMappings = true;
                final boolean printSparql = true;
                //LOGGER.info("Total number of mappings: " + totalNumMappings + "\n");
                //LOGGER.info(bestMappingsPQ.size() + " best mappings:\n");
                List<PatternToQueryMapping> bestMappingsList = new LinkedList<PatternToQueryMapping>();
                int numQuery = 1;
                while (!bestMappingsPQ.isEmpty()) {
                    PatternToQueryMapping nextBestMapping = bestMappingsPQ.remove(0);

                    
                    bestMappingsList.add(nextBestMapping);
                    // store the string description of each mapping in order to be able to display it in the client application
                    nextBestMapping.setStringDescription(nextBestMapping.toString());
                    String stringToDisplay = numQuery++ + ")  trust mark = "
                            + nextBestMapping.getRelevanceMark(userQuery)
                            + "(rMap=" + nextBestMapping.getrMap(userQuery)
                            + " - rPatternCov=" + nextBestMapping.getrPatternCov(userQuery)
                            + " - rQueryCov=" + nextBestMapping.getrQueryCov(userQuery)
                            + " - rNumQueried=" + nextBestMapping.getrNumQueried(userQuery) + ")\n";
                    if (printSentences) {
                        stringToDisplay += " - Sentence: " + nextBestMapping.getSentence(this.kbSparqlServer, userQuery, this.kbConf.lang) + "\n\n";
                    }
                    if (printMappings) {
                        stringToDisplay += " - Mapping: " + nextBestMapping.toString() + "\n\n";
                    }
                    if (printSparql) {
                        stringToDisplay += " - Sparql query:\n" + nextBestMapping.getSparqlQuery(this.kbSparqlServer, userQuery) + "\n\n";
                    }
                    stringToDisplay += "\n";
                    LOGGER.info(stringToDisplay);
                }
                // send me an email with the returned result
//                String message = "pivot query: " + pivotQueryString + "\n\n\n" + "mappings:\n\n";
//                for (PatternToQueryMapping mapping : bestMappingsList) {
//                    message += " + " + mapping.getRelevanceMark() + "\n" + mapping.getSentence() + "\n" + mapping.getSparqlQuery() + "\n\n";
//                }
//                Mail.sendEmail("[SWIP] getBestMappings query " + (nbQuery-1), message);
                
                LOGGER.info("Query processed");

                return bestMappingsList;
            } catch (QueryParsingException ex) {
                LOGGER.info("An error occured while parsing query: " + pivotQueryString + "\n" + ex.getMessage());
                LOGGER.info("Query process aborted");
                LOGGER.error(ex.toString());
            }
        }
        return null;
    }

    /**
     * create query object by parsing the pivot query string representation
     * @param userQuery
     * @return
     * @throws QueryParsingException 
     */
    private Query createQuery(String userQuery) throws QueryParsingException {
        try {
            ANTLRInputStream input = new ANTLRInputStream(new ByteArrayInputStream(userQuery.getBytes()));
            userQueryGrammarLexer lexer = new userQueryGrammarLexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            userQueryGrammarParser parser = new userQueryGrammarParser(tokens);
            return parser.query();
        } catch (RecognitionException ex) {
            throw new QueryParsingException("RecognitionException: " + ex.getMessage());
        } catch (IOException ex) {
            throw new QueryParsingException("IOException: " + ex.getMessage());
        } catch (KeywordRuntimeException ex) {
            throw new QueryParsingException(ex.getMessage());
        } catch (LiteralRuntimeException ex) {
            throw new QueryParsingException(ex.getMessage());
        } catch (SyntaxErrorRuntimeException ex) {
            throw new QueryParsingException("Syntax error at " + ex.getMessage());
        } catch (LexicalErrorRuntimeException ex) {
            throw new QueryParsingException("Syntax error at " + ex.getMessage());
        }
    }

    private void matchQueryElements(Query userQuery, String kbName) {

        //SparqlServer sparqlServer = this.sparqlServers.get(kbName);
        SparqlServer kbSparqlServer = this.kbSparqlServer;
        List<Pattern> patterns = this.patternsMap.get(kbName);

        // first clear previous mappings
        for (List<PatternElement> pes : this.patternElements.values()) {
            for (PatternElement pe : pes) {
                pe.resetForNewQuery();
                this.elementMappings.put(pe, new LinkedList<ElementMapping>());
            }
        }
        // matching step
        userQuery.matchElements(kbSparqlServer);
        // mapping
        for (Pattern p : patterns) {
            p.finalizeMappings(userQuery, kbSparqlServer);
        }
    }

    public String processQuery(String sparqlQuery, String kbName) {

        SparqlServer sparqlServer = this.kbSparqlServer;
        LOGGER.info("process Query : " + kbName);
        LOGGER.info("Query : " + sparqlQuery);
        JSONObject response = new JSONObject();
        ArrayList<JSONObject> queryResults = new ArrayList();
        LOGGER.info("Waiting for sparql server response ... ");
        Iterable<QuerySolution> sols = sparqlServer.select(sparqlQuery);
        LOGGER.info("Response received.");
        for (QuerySolution sol : sols) {
            JSONObject query = new JSONObject();
            Iterator<String> varNames = sol.varNames();
            while (varNames.hasNext()) {
                String varName = varNames.next();
                try {
                    query.put("res", "" + sol.get(varName) + "");
                } catch (JSONException ex) {
                    java.util.logging.Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            queryResults.add(query);
        }
        try {
            response.put("content", queryResults);
        } catch (JSONException ex) {
            java.util.logging.Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
        LOGGER.info("return to client : " + response.toString());
        return response.toString();
    }
}
