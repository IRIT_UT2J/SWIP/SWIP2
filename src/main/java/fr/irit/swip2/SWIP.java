/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.irit.swip2;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.apache.log4j.PropertyConfigurator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sun.jersey.api.container.grizzly2.GrizzlyServerFactory;
import com.sun.jersey.api.core.PackagesResourceConfig;

import fr.irit.swip2.config.ProjectConfigFileParser;
import fr.irit.swip2.utils.EmbeddedFuseki;
import fr.irit.swip2.utils.SourceKB.AbstractSourceKB;
import fr.irit.swip2.utils.SourceKB.DistantSourceKB;
import fr.irit.swip2.utils.SourceKB.LocalSourceKB;
import org.glassfish.grizzly.http.server.HttpServer;

/**
 *
 * @author Fabien
 */
public class SWIP {

    private static final Logger LOGGER = LogManager.getLogger(SWIP.class);
    
    public static HashMap<String, SWIPWorkflow> swList;
    public static final int DEFAULT_PORT = 8080;
    public static final int REF_PORT = 0;
    
    public static final String DEFAULT_IP = "localhost";
    public static final int REF_IP = 1;
    
    public static HashMap<String, SWIPWorkflow> parseProjectsFolder(){
        HashMap<String, SWIPWorkflow> ret =  new HashMap<>();
        File projectFolder = new File("projects");
        for (File fileEntry : projectFolder.listFiles()) {
            if (fileEntry.isDirectory()) {
            	// XXX Doesn't return null if the file does not exist, creates it. Should initialize default values in this case.
                File projectConfigFile = new File(fileEntry.getPath()+File.separator+"project.json");
                LOGGER.debug("Parsing config file : "+projectConfigFile.getAbsolutePath());
                try {
                    ProjectConfigFileParser configParser = new ProjectConfigFileParser(projectConfigFile);                    
                    AbstractSourceKB sourceKB = null;
                    if(configParser.isDistant()){
                        try {
                            sourceKB = new DistantSourceKB(configParser.getProjectName(), configParser.getKbUri());
                        } catch (IOException ex) {
                        	LOGGER.fatal("Distant KB "+configParser.getProjectName()+" unreachable");
                        }
                    }
                    else{
                        sourceKB = new LocalSourceKB(fileEntry.getName(), fileEntry.getPath()+File.separator, configParser.getReasonning(), configParser.getIndexURI());
                    	for(String localKb : configParser.getLocalKbs()){
                        	((LocalSourceKB)sourceKB).loadLocalModel("projects/"+configParser.getProjectName()+"/"+localKb);
                        }
                    	for(String remoteKb : configParser.getRemoteKbs()){
                        	((LocalSourceKB)sourceKB).loadRemoteModel(remoteKb);
                        }
                    	((LocalSourceKB)sourceKB).finalizeKB();
                    }
                    if(sourceKB != null){
                        SWIPWorkflow sw = new SWIPWorkflow(configParser.getConfig(), sourceKB);
                        ret.put(configParser.getProjectName(), sw);
                    }
                } catch (IOException ex) {
                    LOGGER.fatal("Project config file for "+fileEntry.getName()+" dosen't exist (error) : "+ex);
                    ex.printStackTrace();
                }
            }
        }
        
        EmbeddedFuseki fusekiServer = EmbeddedFuseki.getFusekiServer();
        fusekiServer.startServer();
        
        for(SWIPWorkflow sw : ret.values()){
            sw.initWorkFlow();
        }
        
        return ret;
    }
    
    static {
    	// Necessary for jena and fuseki configuration
    	PropertyConfigurator.configure(System.getProperty("log4j.configuration"));
    }
    
    public static void main(String[] args) throws Exception {
        
        swList = SWIP.parseProjectsFolder();
        
//         ArrayList<String> questions = new ArrayList<>();
//        questions.add("Was Keith Richards a member of The Rolling Stones ?");
//        questions.add("Which members of the Beatles are dead?");
//        questions.add("When was Elvis Presley born?");
//        questions.add("How many bands are called Nirvana?");
//        questions.add("Was otter a Spatial Object?");


//        questions.add("Satellite Img with a Platform.");
//
//        for(String question : questions){
//            String results = swList.get("SparkinData").getAnswers(question);
//            System.out.println("QUERY : "+question+" results -> "+results);
//        }   
         
        int port;
        String ip;
        if(args[REF_PORT] != null) {
        	// The port parameter was set
                try{
                    port = Integer.parseInt(args[REF_PORT]);
                }
                catch(Exception ex){
                    port = SWIP.DEFAULT_PORT;
                }
        } else {
        	port = SWIP.DEFAULT_PORT;
        }
        if(args[REF_IP] != null) {
        	// The port parameter was set
        	ip = args[REF_IP];
        } else {
        	ip = SWIP.DEFAULT_IP;
        }
        
        LOGGER.debug("Starting server on port "+port);
        PackagesResourceConfig rc = new PackagesResourceConfig("fr.irit.swip2.WS");
        HttpServer createHttpServer = GrizzlyServerFactory.createHttpServer("http://"+ip+":"+port, rc);
        LOGGER.info("SWIP Server started ! =D");
    }
}
