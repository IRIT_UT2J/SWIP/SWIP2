/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.irit.swip2;

import fr.irit.swip2.config.ProjectConfig;
import fr.irit.swip2.nltopivot.NER.impl.SparqlTextSearchNERParser;
import fr.irit.swip2.nltopivot.parser.impl.StanfordCoreNLPDependenciesParser;
import fr.irit.swip2.nltopivot.pivotRules.PivotGenerator;
import fr.irit.swip2.pivottomappings.PivotToMappings;
import fr.irit.swip2.pivottomappings.controller.Controller;
import fr.irit.swip2.pivottomappings.exceptions.PatternsParsingException;
import fr.irit.swip2.pivottomappings.model.MappingsAnswerFormulation;
import fr.irit.swip2.pivottomappings.model.patterns.Pattern;
import fr.irit.swip2.utils.SourceKB.AbstractSourceKB;
import fr.irit.swip2.utils.sparql.SparqlClient;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.aksw.openqa.AnswerFormulation;
import org.aksw.openqa.manager.plugin.PluginManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.irit.swip2.nltopivot.parser.model.DependencyTree;

/**
 *
 * @author Fabien
 */
public class SWIPWorkflow {
    private static final Logger LOGGER = LogManager.getLogger(SWIPWorkflow.class);
    
    private ProjectConfig config;
    private AnswerFormulation queryProcessor;
    private AbstractSourceKB sourceKB;
    //private HashMap<String, String> queryUris;
    private HashMap<String, MappingsAnswerFormulation> mappings;
    private ArrayList<Pattern> patterns;
    
    SparqlTextSearchNERParser nerParser;
    StanfordCoreNLPDependenciesParser coreNLPDepParser;
    PivotGenerator pivotGen;
    PivotToMappings p2m;
    PluginManager pluginManager;
    
    public SWIPWorkflow(ProjectConfig config, AbstractSourceKB sourceKB){
         
        this.sourceKB = sourceKB;
        this.config = config;
        //this.queryUris = new HashMap<>();
        this.mappings = new HashMap<>();
        this.patterns = new ArrayList<>();
        
        //DBpediaSpotlightNERParser nerParser = new DBpediaSpotlightNERParser();
        this.nerParser = new SparqlTextSearchNERParser(sourceKB);
        this.coreNLPDepParser = new StanfordCoreNLPDependenciesParser();
        this.pivotGen = new PivotGenerator();
        this.p2m = new PivotToMappings(this, sourceKB);
        this.pluginManager = new PluginManager();
        
        // Named entities recognition
        pluginManager.register(nerParser);
        pluginManager.setActive(true, nerParser.getId());
        
        // NLP
        pluginManager.register(coreNLPDepParser);
        pluginManager.setActive(true, coreNLPDepParser.getId());
        
        // Pivot query generation
        pluginManager.register(pivotGen);
        pluginManager.setActive(true, pivotGen.getId());
        
        // Pivot query mapping through patterns
        pluginManager.register(p2m);
        pluginManager.setActive(true, p2m.getId());
        
        this.queryProcessor = new AnswerFormulation(pluginManager);
    }
    
//    public void addQueryUri(String query, String queryUri){
//        System.out.println("ADD URI "+query+" --> "+queryUri);
//        this.queryUris.put(query, queryUri);
//    }
    
    public void addMappingsAnwserFormulation(String query, MappingsAnswerFormulation maf){
        LOGGER.info("ADD MAF : "+maf.getPivotQuery());
        this.mappings.put(query, maf);
    }
    
    public MappingsAnswerFormulation getAnswers(String query){
        
        MappingsAnswerFormulation ret = null;
        try {
            LOGGER.info("Process Query : "+query);
            queryProcessor.process(query);
            LOGGER.info("QUERY processed : "+query);
            
            LOGGER.debug("MAPPINGS : "+this.mappings);
            ret = this.mappings.get(query);
            
            
        } catch (Exception ex) {
            LOGGER.error("Some errors occured during the query processing ... "+ex);
            LOGGER.error(ex);
        }
        LOGGER.debug("MAF : "+ret);
        return ret;
    }
    
    public String getPivotQuery(DependencyTree dt){
        return this.pivotGen.dependenciesToPivot(dt, "en");
    }
    
    public HashMap<String, String> getNER(String query){
        return this.nerParser.getNERFromQuery(query);
    }
    
    public DependencyTree getDependenciyTree(String query){
        return this.coreNLPDepParser.getDependenciesGraph(query);
    }
    
    
    
    public JSONObject toJSON(){
    	ObjectMapper mapper = new ObjectMapper();
    	StringWriter sw = new StringWriter();
    	JSONObject response=null;
    	try {
			mapper.writeValue(sw, this.config);
	        response = new JSONObject(sw.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
        return response;
    }
    
    
    public void setPatterns(ArrayList<Pattern> patterns){
        this.patterns =patterns;
    }
    
    public ArrayList<Pattern> getPatterns(){
        if(this.patterns == null || this.patterns.isEmpty()){
            Controller.getInstance(this.config.getProjectName());
        }
        return this.patterns;
    }
    
    public void setUpdatedPatterns(ArrayList<Pattern> patterns) throws PatternsParsingException{
        this.patterns = patterns;
        Controller cont = Controller.getInstance(this.config.getProjectName());
        cont.computeLoadedPatterns(this.patterns);
    }
    
    public void initWorkFlow(){
        this.sourceKB.initSource();
    }
    public String getKBUri(){
        return this.sourceKB.getSourceKBUrl();
    }
    public String getProjectName(){
        return this.config.getProjectName();
    }
    public String getProjectDesc(){
        return this.config.getProjectDesc();
    }
    
    public JSONObject sparqlQuery(String sparqlQuery){
        SparqlClient sc = new SparqlClient("http://localhost:3031/"+this.config.getProjectName());
        return sc.selectJSON(sparqlQuery);
    }
}
